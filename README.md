<a name="readme-top"></a>
<!-- [![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][star-url]
[![Issues][issues-shield]][issues-url]
[![CeCIll License][license-shield]][license-url] -->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/monvilre/tocco">
    <img src="img/Logo/logo_tocco.png" alt="Logo" width="175.0" height="89.8">
  </a>

<!-- <h3 align="center">ToCCo</h3> -->

<div align="left" justify=True>

ToCCo for “Topographic Coupling at Core-Mantle interface”, is a local perturbative model that calculates the flow over a topography or between two boundaries. It solves Magneto-Hydro-Dynamic equations in a Cartesian Boussinesq frame and can take into consideration: rotation, magnetic field, stratification, and fluid viscosity.
ToCCo is coded in Python language combining symbolic mathematics with the [SymPy](www.sympy.org) library  and numerical evaluation with the [mpmath](mpmath.org) library , which provides arbitrary-precision floating-point calculation.

</div>

[![python][python-shield]][python-url][![sympy][sympy-shield]][sympy-url]




<div align="center">
  <a href="https://gitlab.com/monvilre/tocco">
    <img src="img/flow_Hartmann_layer.png" alt="Hartmann" height="300">
    <img src="img/snapshot4.png" alt="Hartmann" height="300">
  </a>
</div>
<!-- GETTING STARTED -->

## Getting Started


### Installation
<div align="left" justify=True>

ToCCo is coded in python 3 and required some additional packages.
* [Sympy](https://www.sympy.org)
* [mpmath](mpmath.org/) (included in Sympy package)
* [numpy](https://numpy.org/)
* [matplotlib](https://matplotlib.org/)

The easiest way is to use [Conda](https://docs.conda.io/en/latest/) packages manager to install all these packages and their dependencies.

  ```sh
  conda install sympy numpy matplotlib
  ```

Then get the repo:
  ```sh
  git clone https://gitlab.com/monvilre/tocco
  ```


</div>

<!-- USAGE EXAMPLES -->
### Usage

<div align="left" justify=True>

To use ToCCo modify the parameter file `params.py` to set up your calculations.

Then start the code using
  ```sh
  python ./ToCCo.py
  ```
it using the parameter file available in the current file.

if you have chosen to save your calculation, it will be stored as a binary file `dat`. These files can be opened with the built-in python package [Pickle](https://docs.python.org/3/library/pickle.html) or via the provided library `Viz_ToCCo.py` which allows you to load the files easily but also to produce figures.

If needed, the file `script_pop.py` is an example of a parallelization script for calculation on multiple cores for a given range of parameters.

</div>

<!-- LICENSE -->
## License

<div align="left" justify=True>

ToCCo is distributed under the CeCILL License, Everybody is free to use, modify and contribute. See `LICENSE.txt` for more information.

</div>



<!-- CONTACT -->
## Contact

<div align="left" justify=True>

[![email][mail-shield]][mail-url]
[![scholar][scholar-shield]][scholar-url]
[![gate][gate-shield]][gate-url]
[![gate][orcid-shield]][orcid-url]
[<img alt="Twitter" src="https://img.shields.io/badge/remyMonville-%231DA1F2.svg?style=for-the-badge&logo=Twitter&logoColor=white" />](https://twitter.com/remyMonville)
[<img alt="Gitlab" src="https://img.shields.io/badge/monvilre-FC6D26.svg?style=for-the-badge&logo=gitlab&logoColor=white" />](https://gitlab.com/monvilre)

</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

<div align="left" justify=True>

* This code has been produced during my PhD Thesis "Topographic coupling and waves dynamics in planetary cores" at the ISTerre lab (CNRS, Université Grenoble Alpes, St Martin d'Heres, France).
* This work has been funded by the European Research Council (ERC) under the European Union's Horizon 2020 research and innovation programme via the [*THEIA*](https://www.isterre.fr/english/research-observation/teams-1105/geodynamo/means-and-tools/on-going-projects/erc-project-theia) project (grant agreement no. 847433).

</div>

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[python-shield]: https://img.shields.io/badge/Python3-3776AB?style=flat-square&logo=Python&logoColor=white
[python-url]: https://scholar.google.fr/citations?user=NwCJ_ZMAAAAJ&hl=fr

[sympy-shield]: https://img.shields.io/badge/Sympy-3B5526?style=flat-square&logo=SymPy&logoColor=white
[sympy-url]: https://scholar.google.fr/citations?user=NwCJ_ZMAAAAJ&hl=fr



[scholar-shield]: https://img.shields.io/badge/Google_Scholar-4285F4?style=for-the-badge&logo=google-scholar&logoColor=white
[scholar-url]: https://scholar.google.fr/citations?user=NwCJ_ZMAAAAJ&hl=fr

[mail-shield]: https://img.shields.io/badge/remy\.monville@univ--grenoble--alpes\.fr-FF9E0F?style=for-the-badge
[mail-url]: mailto:remy.monville@univ-grenoble-alpes.fr

[gate-shield]: https://img.shields.io/badge/Research_Gate-00CCBB?style=for-the-badge&logo=ResearchGate&logoColor=white
[gate-url]: https://www.researchgate.net/profile/Remy-Monville

[orcid-shield]: https://img.shields.io/badge/ORCID-A6CE39?style=for-the-badge&logo=ORCID&logoColor=white
[orcid-url]: https://orcid.org/my-orcid?orcid=0000-0002-9460-2293

[contributors-shield]: https://img.shields.io/gitlab/contributors/monvilre/tocco?style=for-the-badge
[contributors-url]: https://gitlab.com/monvilre/tocco/-/graphs/main?ref_type=heads
[forks-shield]: https://img.shields.io/gitlab/forks/monvilre/tocco.svg?style=for-the-badge
[forks-url]: https://gitlab.com/monvilre/tocco/-/forks
[stars-shield]: https://img.shields.io/gitlab/stars/monvilre/tocco.svg?style=for-the-badge
[stars-url]: https://gitlab.com/monvilre/tocco/starrers
[issues-shield]: https://img.shields.io/gitlab/issues/open/monvilre/tocco?style=for-the-badge
[issues-url]: https://gitlab.com/monvilre/tocco/main/issues
[license-shield]: https://img.shields.io/gitlab/license/monvilre/tocco.svg?style=for-the-badge
[license-url]: https://gitlab.com/monvilre/tocco/blob/main/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
[product-screenshot]: images/screenshot.png
[Next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[Next-url]: https://nextjs.org/
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
[Angular.io]: https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white
[Angular-url]: https://angular.io/
[Svelte.dev]: https://img.shields.io/badge/Svelte-4A4A55?style=for-the-badge&logo=svelte&logoColor=FF3E00
[Svelte-url]: https://svelte.dev/
[Laravel.com]: https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 
