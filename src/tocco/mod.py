from sympy import *
from sympy.vector import CoordSys3D, curl, gradient, divergence, Del, laplacian, matrix_to_vector, VectorZero,Vector
C = CoordSys3D('C')
x = C.x
y = C.y
z = C.z
x.is_real
y.is_real
z.is_real
x._assumptions['real'] = True
y._assumptions['real'] = True
z._assumptions['real'] = True
t = Symbol('t', real=True)
dicprint = {x:Symbol('x'),y:Symbol('y'),z:Symbol('z')}
Dist, et, ev = symbols("Dist,epsilon_t,epsilon_v", real=True,positive=True)
Ri, Ro, Al, Rm, omega,  kx, ky, xl, kyl, kz, rho_r, alpha, g, zeta, g0, g1, buoy, a, b, c, dv, fv, BOx, BOy, BOz, psi0, psi0_2b, p0,rho0 = symbols(
    "Ri, Ro, Al, Rm, omega, kx,ky,kxl,kyl,kz,rho_r,alpha, g,zeta, g0, g1, buoy, a, b, c, dv, fv,BOx, BOy, BOz,psi0,psi0_2b,p0,rho0",real=True,positive=True)
qRm, qRe, qRo, qFr, chi, qAl, qRmm, qRmc, Rl,fC,qRer = symbols(
    "qRm,qRe,qRo,qFr,chi,qAl,qRmm,qRmc,Rl,fC,qRer")
THETA,PHI = symbols("THETA,PHI",real=True,positive=True)
bmx, bmy, bmz = symbols("bmx,bmy,bmz")
f0 = Function("f0")(x, y, z, t)
f1 = Function("f1")(x, y, z, t)
f2 = Function("f2")(x, y, z, t)
f0_2 = Function("f0_2")(x, y, z, t)
f1_2 = Function("f1_2")(x, y, z, t)

b0x = Function("b0x")(x, y, z, t)
b0y = Function("b0y")(x, y, z, t)
b0z = Function("b0z")(x, y, z, t)
u0x = Function("u0x")(x, y, z, t)
u0y = Function("u0y")(x, y, z, t)
u0z = Function("u0z")(x, y, z, t)

def taylor(exp, nv, nt, dic):
    """Taylor expansion in 0, with two variables. Return Taylor coefficient
    of order nv/nt. (you need to multiply output with ev**nt*et**et to have
    the complete Taylor term)

    Attrs:
    - exp: Expr.
    - nv: int, order associated to small parameter ev.
    - nt: int, order associated to small parameter et.
    - dic: dict, dictionary of values to replace in Expr

    Returns:
    - Expr."""

    if nv == 0 and nt == 0:
        expt = exp.xreplace({ev: 0, et: 0})
        return(expt)
    if nt == 0:
        df = diff((exp.xreplace(dic)), ev, nv).xreplace({ev: 0, et: 0})
    elif nv == 0:
        df = diff((exp.xreplace(dic)), et, nt).xreplace({ev: 0, et: 0})
    else:
        df = diff((exp.xreplace(dic)), ev, nv,
                  et, nt).xreplace({ev: 0, et: 0})
    expt = Rational(1 / (factorial(nt) * factorial(nv))) * df
    return expt

def AgradB(A,B):
    aB = (((A&C.i) * diff(B&C.i, x) + (A&C.j) * diff(B&C.i, y) + (A&C.k) * diff(B&C.i, z)) * C.i
            + ((A&C.i) * diff(B&C.j, x) + (A&C.j) * diff(B&C.j, y) + (A&C.k) * diff(B&C.j, z)) * C.j
            + ((A&C.i) * diff(B&C.k, x) + (A&C.j) * diff(B&C.k, y) + (A&C.k) * diff(B&C.k, z)) * C.k)
    return(aB)
