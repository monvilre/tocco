# coding: utf-8
import sys
import shutil
import os
import numpy as np
import glob
import tempfile
import subprocess
import multiprocessing
import mpmath as mp
from sympy import *

prec=100
mp.mp.dps = prec
order = 1
Bound_nb = 2
condB = "harm pot"
condU = "Inviscid"


# vars
theta = np.pi/4
psi = 0
qFr = 72900.0
qRo = 72900.0
qRm = 0.08
qRmm = 80.0
qRmc = 0
qAl = 44.60310290381928
omega = 0
qRe = 0
D = 1
Rl = 0.02868617326448652


# meshgrid

u0x = "mp.sin({theta})"
u0y = 0
u0z = 0
b0x =
b0y =  "(mp.sin({theta}) - 2*mp.cos({theta})*Rl*y)/2"
b0z =  "-(mp.cos({theta})*( 1 - {Rl}*z ) + {Rl} * mp.sin({theta}) * y)"
fC =  "(mp.cos({theta})*( 1 + 2*{Rl}*z ) + {Rl} * mp.sin({theta}) * y) * C.k  + (mp.sin({theta}) - 2*mp.cos({theta})*Rl*y) * C.j"
f1 = "-(exp(I * (x)) + exp(-I * (x))) / 2"
f1_2 = "0"









index =
#template of params
template = f'''
#! /usr/bin/env python3
# coding: utf-8
"""This is a parameter file for ToCCo"""

__author__ = "Rémy Monville"
__email__ = "remy.monville@univ-grenoble-alpes.fr"

### Import ###
import mpmath as mp
from sympy import exp, symbols, I, Symbol, Function, sqrt, conjugate
from sympy import Float
from sympy.vector import CoordSys3D
import sys
import os
# os.environ['SYMPY_GROUND_TYPES'] = 'gmpy'
os.environ['SYMPY_CACHE_SIZE'] = '100000'
C = CoordSys3D('C')
x = C.x
y = C.y
z = C.z
x.is_real
y.is_real
z.is_real
x._assumptions['real'] = True
y._assumptions['real'] = True
z._assumptions['real'] = True
t = Symbol('t', real=True)

Dist,ev,et = symbols('Dist,ev,et',real=True)

alpha, kxl, kyl, g0, omega, BOx, BOy, BOz, zeta, Ri, p0,kx,ky,kz,psi0,psi0_2b,p0 = symbols(
"""alpha,kxl,kyl,g0,omega,BOx,BOy,BOz,zeta,Ri,p0,kx,ky,kz,psi0,psi0_2b,p0""")
U0, qRm, qRmm, qRmc, qRe, qRo, qFr, chi, qAl, Rl,fC = symbols(
"U0,qRm,qRmm,qRmc,qRe,qRo,qFr,chi,qAl,Rl,fC")
b0x = Function("b0x")(x, y, z, t)
b0y = Function("b0y")(x, y, z, t)
b0z = Function("b0z")(x, y, z, t)
u0x = Function("u0x")(x, y, z, t)
u0y = Function("u0y")(x, y, z, t)
u0z = Function("u0z")(x, y, z, t)
f0 = Function("f0")(x, y, z, t)
f1 = Function("f1")(x, y, z, t)
f2 = Function("f2")(x, y, z, t)
rho0 = Function("rho0")(x, y, z, t)
f0_2 = Function("f0_2")(x, y, z, t)
f1_2 = Function("f1_2")(x, y, z, t)
f2_2 = Function("f2_2")(x, y, z, t)
############
prec = {prec}
order = {order}
Bound_nb = {Bound_nb}
mp.mp.dps = prec

condB = {condB}  # "harm pot", "Thick"
condU = {condU}  # Inviscid , Stressfree

Secular = False

buf = 0
atmo = 0
if atmo == 1:
    condB = "harm pot"
test = 0
######  Post process #####
calculate_stress = True
ohmic_dissipation = False
decomp_kz= False
write_file = True

filename = "./output_{index}" '''




#Define the template of params
def MakeFile(path,file_name,theta,phi,vars):
    Varstr = ["theta","psi","qFr","qRo","qRm","qRmm","qRmc","qAl","omega","qRe","Rl","D"]
    theta,psi,qFr,qRo,qRm,qRmm,qRmc,qAl,omega,qRe,Rl,D = np.meshgrid(theta,psi,qFr,qRo,qRm,qRmm,qRmc,qAl,omega,qRe,Rl,D)

    dicVars =
    dico = f'''
    DICO = {{
    u0x: {u0x},
    u0y: {u0y},
    u0z: {u0z},
    b0x: {b0x},
    b0y: {b0y},
    b0z: {b0z},
    rho0: (1 - z),
    fC: {fC},
    f0: z,
    f1: {f1},
    f0_2: z,
    f1_2: {f1_2},
    U0: 1,
    '''



    temp_path = path + file_name
    with open(file_name, 'w') as f:
        f.write( template + f'''
    DICO = {{
    u0x: {u0x},
    # u0x: ev* (exp(I * omega * t) + exp(-I * omega * t)) / 2,
    u0y:0,
    u0z: 0,
    b0x: 0,
    # b0y: 0,
    # b0z: 1,
    # b0y: mp.sin(THETA)/2,
    # b0z: -mp.cos(THETA),
    b0y: (mp.sin(THETA) - 2*mp.cos(THETA)*Rl*y)/2,
    b0z: -(mp.cos(THETA)*( 1 - Rl*z ) + Rl * mp.sin(THETA) * y),

    rho0: (1 - z),
    fC: (mp.cos(THETA)*( 1 + 2*Rl*z ) + Rl * mp.sin(THETA) * y) * C.k  + (mp.sin(THETA) - 2*mp.cos(THETA)*Rl*y) * C.j,
    # fC: mp.cos(THETA) *C.k,

    f0: z,
    # f1: -(exp(I * (x)) + exp(-I * (x))) / 2,
    # f1: -(exp(I*(x+y)/(mp.sqrt(2)))+exp(-I*(x+y)/(mp.sqrt(2)))+exp(I*(x-y)/(mp.sqrt(2)))+exp(-I*(x-y)/(mp.sqrt(2))))/4,
    # f1: -(exp(I*x)+exp(I*y)+exp(-I*x)+exp(-I*y))/4,
    f1: -(exp(I*(x+y))+exp(-I*(x+y))+exp(I*(x-y))+exp(-I*(x-y)))/4,
    f0_2: z,
    f1_2: 0,
    # f1_2: -(exp(I*x)+exp(I*y)+exp(-I*x)+exp(-I*y))/4,
    U0: 1,
    qRe: 0,
    omega: mp.mpf('2318.9142857142856'),
    qRo:mp.mpf ('72900'),
    Rl: mp.mpf('0.02868617326448652'),
    qRmc: mp.nan,
    qRmc: mp.mpf('75'),
    qAl: mp.mpf('44.60310'),
    qRm: mp.mpf('0.08'),
    qRmm: mp.mpf('7.957747154594769e-06'),
    qFr: mp.mpf('72900'),
    Dist: {var}
}}
DICO = Glane
'''
)
print('File created')

ph,th = [0],[np.pi/4]

Dist = np.logspace(-8,-4,48)
PHI,THETA,DI = np.meshgrid(ph,th,Dist)


THETA = THETA.flatten()
PHI = PHI.flatten()
VAR = DI.flatten()


print('number of points :', len(THETA))

N_core = 16
Tocco_dir = '/nfs_scratch/monvilre/tocco/ToCCo.py'
python_dir = '/home/monvilre/anaconda3/bin/python3'
#Tocco_dir = '/home/monvilre/Documents/Thesis/tocco_vorticity_fork/tocco/ToCCo.py'
#python_dir = 'python'
# Parent Directory path
parent_dir = os.getcwd()

existing_folders = glob.glob("output*")
# Mode -S
existing_folders = []
if len(existing_folders) != 0:
    cont  = input(" Continue ? Erase all files ? Quit (Y/R/N)")
    if cont == "R":
        for fold in existing_folders:
         shutil.rmtree(fold)
    elif cont == "N":
        sys.exit()
    elif cont == "Y":
        print("continuing")
    else :
        sys.exit()

directories = []
for theta,phi,var in zip(THETA,PHI,VAR):
    directory = "output_Dist_"+str(var)+"_"+str(theta)
    path = os.path.join(parent_dir, directory)
    os.makedirs(path,exist_ok=True)
    print("Directory '% s' created" % directory)
    os.chdir(path)

    file_name= "params.py"
    MakeFile(path,file_name,theta,phi,var)
    os.chdir('../')
    directories += [directory]

print('finished')




print('directories',directories)
# processes = []
# for dir in directories:
#     f = tempfile.TemporaryFile()
#     p = subprocess.Popen(['python', Tocco_dir],stdout = f,cwd = dir)
#     processes.append(p)
from tqdm import tqdm


def func(index):
    f = tempfile.TemporaryFile()
    process = subprocess.Popen([python_dir, Tocco_dir],stdout = f,cwd = directories[index])
    process.wait()

if __name__ == '__main__':
    # p = multiprocessing.Pool(multiprocessing.cpu_count()) ## all core you dispose
    p = multiprocessing.Pool(N_core) ## arbitrary number of simultaneous process
    # p.map(func, range(0,len(directories)))

    max_ = len(directories)
    with  multiprocessing.Pool(N_core) as p:
        with tqdm(total=max_) as pbar:
            for i, _ in enumerate(p.imap_unordered(func, range(0, max_))):
                pbar.update()
