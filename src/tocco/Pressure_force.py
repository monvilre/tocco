#! /usr/bin/env python3
# coding: utf-8
""" This tool compute the pressure torque of a ToCCo Output. use comp_Press()
function to use it inside a script, it can be also used with command line as :
python Pressure_force.py [output.dat] [dimensionless topo height]"""

__author__ = "Rémy Monville"
__email__ = "remy.monville@univ-grenoble-alpes.fr"

import sys
from sympy import *
from sympy.vector import CoordSys3D, curl, gradient, divergence, Del, Divergence, Gradient, laplacian, Curl, matrix_to_vector,Vector
import numpy as np
import pickle
import mpmath as mp

from rich import print as rprint
from rich.panel import Panel
from rich.text import Text
# from sympy.vector import CoordSys3D
# from sympy import init_printing
# init_printing() 

# C = CoordSys3D('C')
# x = C.x
# y = C.y
# z = C.z
# x.is_real
# y.is_real
# z.is_real
# x._assumptions['real'] = True
# y._assumptions['real'] = True
# z._assumptions['real'] = True
# Dist, et, ev = symbols("Dist,epsilon_t,epsilon_v", real=True)
# Ri, Ro, Al, Rm, omega,  kx, ky, kxl, kyl, kz, rho_r, alpha, g, zeta, g0, g1, buoy, a, b, c, dv, fv, BOx, BOy, BOz, psi0, psi0_2b, p0 = symbols(
#     "Ri, Ro, Al, Rm, omega, kx,ky,kxl,kyl,kz,rho_r,alpha, g,zeta, g0, g1, buoy, a, b, c, dv, fv,BOx, BOy, BOz,psi0,psi0_2b,p0")
# U0, qRm, qRe, qRo, qFr, chi, qAl, qRmm, qRmc, Rl,fC = symbols(
#     "U0,qRm,qRe,qRo,qFr,chi,qAl,qRmm,qRmc,Rl,fC")
from tocco.mod import *
from itertools import product


# def taylor(exp, nv, nt, dic):

#     if nv == 0 and nt == 0:
#         expt = exp.xreplace({ev: 0, et: 0})
#         return(expt)
#     if nt == 0:
#         df = diff((exp.xreplace(dic).doit()), ev, nv).xreplace({ev: 0, et: 0})
#     elif nv == 0:
#         df = diff((exp.xreplace(dic).doit()), et, nt).xreplace({ev: 0, et: 0})
#     else:
#         df = diff((exp.xreplace(dic).doit()), ev, nv,
#                   et, nt).xreplace({ev: 0, et: 0})

#     expt = Rational(1 / (factorial(nt) * factorial(nv))) * df
#     return expt


def taylor_serie(exp, nvmax, ntmax, dic):
    nv = np.arange(nvmax + 1)
    nt = np.arange(ntmax + 1)
    expt = 0
    for v in nv:
        for t in nt:
            # print('order (',v,t,')')
            tay = taylor(exp, v, t, dic) * et**t * ev**v
            expt += tay
            print(str(v + t) + '/' + str(nvmax + ntmax), end='\r')
    return(expt)


def taylor_s_integral(exp, nvmax, ntmax, dic):
    from scipy import integrate
    nv = np.arange(nvmax + 1)
    nt = np.arange(ntmax + 1)
    expt = 0
    for v in nv:
        for t in nt:
            # print('order (',v,t,')')
            tay = taylor(exp, v, t, dic)
            ltay = lambdify(z, tay)
            intay, err = integrate.quad(ltay, 0, -np.inf)
            expt += intay * et**t * ev**v
            print(str(v + t) + '/' + str(nvmax + ntmax), end='\r')
    return(expt)

def strain(U):
    """Return the strain-tensor of velocity field U.

    Attrs:
    - Vector.

    Returns:
    - Sympy Matrix.
    """
    xy = 1 / 2 * (diff(U & C.i, y) + diff(U & C.j, x))
    xz = 1 / 2 * (diff(U & C.i, z) + diff(U & C.k, x))
    yz = 1 / 2 * (diff(U & C.j, z) + diff(U & C.k, y))
    strainT = Matrix([[diff(U & C.i, x), xy, xz],
                      [xy, diff(U & C.j, y), yz],
                      [xz, yz, diff(U & C.k, z)]])
    return(strainT)


def int_curv(Exp, f, coeff, orders):
    Exp = surfcond(Exp, f, {})* coeff
    nv = orders[:, 0][:, 0]
    nt = orders[:, 0][:, 1]
    expt = Exp*0
    for vv,tt in zip(nv,nt):
        print("orders",vv,tt)
        tay = expand(taylor(Exp, vv, tt, {}))
        print("tay",tay)
        va = list(findexp(tay))
        # keep t dependency
        va = list(findexp(expand(tay.xreplace({t:0}).evalf())))
        def inde(M): return expand(M,force=True).as_independent(*va)[0]
        try:
            if isinstance(tay, MatrixExpr) == True:
                intay = tay.applyfunc(inde)
            elif tay == 0:
                intay=0
            else:
                intay = inde(tay)

            expt += intay* et**tt * ev**vv

        except Exception as e:
            print("Exception occurred -->  "+ repr(e))
            print(type(tay))
            pass
    expt = simplify(expt)

    return(expt)

# def int_curv_laty(Exp, f, orders):
#     nv = orders[:, 0][:, 0]
#     nt = orders[:, 0][:, 1]

#     expt = Exp*0
#     for vv,tt in zip(nv,nt):
#         print('     order (',vv,tt,')', end='\r')
#         tay = taylor(Exp, vv, tt, {})
#         va = [exp(A*x*I) for A in [-4,-2,-1,1,2,4]]
#         def inde(M): return expand(integrate(M,(z,0,100))).as_independent(*va)[0]*2*pi
#         intay = tay.applyfunc(inde)
#         expt += intay* et**tt * ev**vv
#     return(simplify(expt))

# def int_curv_latx(Exp, f, orders):
#     nv = orders[:, 0][:, 0]
#     nt = orders[:, 0][:, 1]

#     expt = Exp*0
#     for vv,tt in zip(nv,nt):
#         print('     order (',vv,tt,')', end='\r')
#         tay = taylor(Exp, vv, tt, {})
#         va = [exp(A*y*I) for A in [-4,-2,-1,1,2,4]]
#         def inde(M): return expand(M).as_independent(*va)[0]

#         intay = tay.applyfunc(inde)
#         intay = integrate(intay,(z,0,100))
#         expt += intay* et**tt * ev**vv
#     # print('end : ', simplify(expt).evalf(5))
#     return(simplify(expt))

def int_curv_z(Exp, f, detJ, orders,zmin,zmax,tt):
    # Exp = sympify(Exp)
    Exp = Exp.xreplace({z: -f + 2 * z}) * detJ

    nv = orders[:, 0][:, 0]
    nt = orders[:, 0][:, 1]
    expt = 0
    for vv,tt in zip(nv,nt):
        tay = expand(taylor(Exp, vv, tt, {}))
        va = list(findexp(Matrix([expand(tay.xreplace({t:0,z:0}).evalf())])))
        def inde(M): return M.as_independent(*va)[0]
        try:
            if tay == 0:
                intay = 0
            else:
                intay = inde(tay)
            intay = integrate(intay,(z, zmin, zmax))
            expt += intay* et**tt * ev**vv
        except Exception as e:
            print("Exception occurred -->  "+ repr(e))
            print(type(tay))
            pass
    return(expt)


def simp(expr):
    """Best simplification to devellop properly exponential form.

    Attrs:
    - Expr.

    Returns:
    - Expr.
    """
    expe = powsimp(expand(expr))
    return(expe)

# def simptay(Exp,prec,orders):
#     nv = orders[:, 0][:, 0]
#     nt = orders[:, 0][:, 1]
#     print(nv)
#     print(nt)
#     expt = 0
#     for vv,tt in zip(nv,nt):
#         print('     order (',vv,tt,')')
#         tay = taylor(Exp, vv, tt, {})
#         expt += N(simp(tay),prec,chop=True)* et**tt * ev**vv
#     return(expt)



def conjv(vect, C):
    ve = matrix_to_vector(((vect).to_matrix(C).H).xreplace({conjugate(x): x, conjugate(
        y): y, conjugate(z): z, conjugate(t): t, conjugate(et): et, conjugate(ev): ev}), C)
    return(ve)

def conj(scal):
    ve = conjugate(scal).xreplace({conjugate(x): x, conjugate(y): y, conjugate(
        z): z, conjugate(t): t, conjugate(et): et, conjugate(ev): ev})
    return(ve)

def realve(vect, C):
    ve = (vect + conjv(vect, C)) / 2
    return(ve)

def surfcond(val, f, dic):
    va = val.xreplace(dic).doit().xreplace(
        {z.xreplace(dic): -(f - z)}).xreplace(dic)
    # va = val.xreplace(dic).doit().xreplace(
    #     {z.xreplace(dic): 0}).xreplace(dic)
    return(va)

def findexp(Ma):
    """Find all unique exponential forms of Ma (/!\ without any simplification of input).

    Attrs:
    - Sympy Matrix.

    Returns:
    - set of Sympy Expression.
    """
    Ma=list(Ma)
    def coe(x): return list(x.find(exp))
    expo = [coe(ma) for ma in Ma]
    expo = set([val for sublist in expo for val in sublist])
    return(expo)


def comp_Diss(solfull,C,f,qRm,qRmm,orders,condB,dic):
    C = CoordSys3D('C')
    global x
    global y
    global z
    global t
    global et
    global ev
    x = C.x
    y = C.y
    z = C.z
    et,ev,t = symbols('epsilon_t,epsilon_v,t', real=True)
    rprint(Panel(Text(f"||| Computation of Dissipation |||", justify="center"),border_style = 'yellow'))
    li = list(product(np.arange(0,orders[0]*2+1),np.arange(0,orders[1]*2+1)))
    orders = np.array([[list(i)] for i in li])
    rprint(f'Orders : {[list(ord[0]) for ord in orders]}')
    B = solfull[2]['OC'] 
    psi = solfull[2]['M1'] 
    P = (solfull[1])

    U = solfull[0]

    J = realve(curl(B),C)
    Phi = simp(qRm * ((J).dot(J))).xreplace(dic)  #- (cuB).dot(U.cross(B))
    Jm = realve(curl(psi),C)
    Phi_m = simp(qRmm * ((Jm).dot(Jm))).xreplace(dic)

    Theta = Matrix([x, y, -f+z])
    surf_coeff = (diff(Theta, x).cross(diff(Theta, y)))
    surf_coeff = sqrt(surf_coeff[0]**2 + surf_coeff[1]**2 + surf_coeff[2]**2)
    detJ = surf_coeff


    Diss_f = int_curv_z(Phi, f, detJ, orders,-oo,0,tt=0)
    Diss_m  = int_curv_z(Phi_m, f, detJ, orders,0,oo,tt=0)
    Diss = [N(Diss_f,mp.mp.dps),N(Diss_m,mp.mp.dps)]
    print('Diss fluid = ')
    pprint(Diss[0].evalf(5))
    print('Diss solid = ')
    pprint(Diss[1].evalf(5))
    return(Diss)

def calc_stress(solfull, C, f, orders,condB,DIC,Decomp_kz,Dm=1e-3):
    C = CoordSys3D('C')
    global x
    global y
    global z
    global t
    global et
    global ev
    x = C.x
    y = C.y
    z = C.z
    et,ev,t = symbols('epsilon_t,epsilon_v,t', real=True)
    qRm, qRmm, Rl, qRo = symbols('qRm,qRmm,Rl,qRo')
    rprint(Panel(Text(f"||| Computation of Stress |||", justify="center"),border_style = 'yellow'))
    ### LIST OF ORDERS

    li = list(product(np.arange(0,orders[0]*2+1),np.arange(0,orders[1]*2+1)))
    orders = np.array([[list(i)] for i in li])
 
    rprint(f'Orders : {[list(ord[0]) for ord in orders]}')

    ### SURFACE 
    Theta = Matrix([x, y, -f+z])
    surf_coeff = (diff(Theta, x).cross(diff(Theta, y)))
    surf_coeff = sqrt(surf_coeff[0]**2 + surf_coeff[1]**2 + surf_coeff[2]**2)
    nn = gradient(f).normalize()

    ### variables
    B1 = solfull[2]['OC'] 
    P = (solfull[1])
    B = solfull[2]['M1']
    U = solfull[0]

    def comp(B,p,U,nn):
        VFpm = 0
        VFM = 0

        ### viscous stress
        Fv = matrix_to_vector(-(2*qRe.xreplace(DIC)*strain(U) * nn.to_matrix(C)),C)
        #Fv = (matrix_to_vector((qRe.xreplace(DIC)*strain(U) * nn.to_matrix(C)),C))#.cross(nn))
        VFv = int_curv(Fv.to_matrix(C), f, surf_coeff, orders)
        print('VFv')
        pprint(N(VFv,mp.mp.dps).evalf(5).T)


        ### Magnetic pressure
        Fpm = -((B.dot(B) / 2) * nn)
        VFpm = int_curv(Fpm.to_matrix(C), f, surf_coeff, orders)#-int_curv(Fpm, f-1e5, -surf_coeff, orders)
        print('VFpM')
        pprint(N(VFpm,mp.mp.dps).evalf(5).T)


        ### Magnetic force
        FM = ((nn.dot(B)) * (B))
        VFM = int_curv(FM.to_matrix(C), f, surf_coeff, orders)#-int_curv(FM, f-1e5, surf_coeff, orders)
        print('VFM')
        pprint(N(VFM,mp.mp.dps).evalf(5).T)


        # ### Pressure force
        Fp = (p * nn)
        VFp = int_curv(Fp.to_matrix(C), f, surf_coeff, orders)
        print('VFp')
        pprint(N(VFp,mp.mp.dps).evalf(5).T)

        return(VFv,VFpm,VFp,VFM)

    if Decomp_kz ==True:
        expo = findexp(Matrix([P,B]))
        VFv,VFpm,VFp,VFM = np.zeros(len(expo),dtype = object),np.zeros(len(expo),dtype = object),np.zeros(len(expo),dtype = object),np.zeros(len(expo),dtype = object)
        for ind,ex in enumerate(expo):
            P_ex = P.coeff(ex)*ex
            B_ex = ((B&C.i).coeff(ex)*C.i + (B&C.j).coeff(ex)*C.j + (B&C.k).coeff(ex)*C.k)*ex
            U_ex  = ((U&C.i).coeff(ex)*C.i + (U&C.j).coeff(ex)*C.j + (U&C.k).coeff(ex)*C.k)*ex
            rVFv,rVFpm,rVFp,rVFM = comp(B_ex,P_ex,U_ex,nn)
            VFv[ind] = rVFv
            VFpm[ind] = rVFpm
            VFp[ind] = rVFp
            VFM[ind] = rVFM
        expo = np.array(list(expo),dtype=object)
        norm = np.array([M.xreplace({et:1e-3}).norm() for M in VFp],dtype = np.float64)+np.array([M.xreplace({et:1e-3}).norm() for M in VFpm],dtype = np.float64) + np.array([M.xreplace({et:1e-3}).norm() for M in VFM],dtype = np.float64)
        try:
            norm = np.abs(norm)/np.nanmax(np.abs(norm))
            kzs = [expand(log(ex),force = True).coeff(z) for ex in expo]

            print(kzs)
            print(norm)
        except:
            kzs=0
            norm=0

    VFv,VFpm,VFp,VFM = comp(B,P,U,nn)
    print('Total')
    tot = N((VFv+VFp+ VFpm+ VFM),mp.mp.dps)
    pprint(tot.evalf(5).T)

    if Decomp_kz == True:
        return((kzs,norm),[VFv.evalf(mp.mp.dps),VFp.evalf(mp.mp.dps),VFpm.evalf(mp.mp.dps),VFM.evalf(mp.mp.dps),tot])
    else:
        return([VFv.evalf(mp.mp.dps),VFp.evalf(mp.mp.dps),VFpm.evalf(mp.mp.dps),VFM.evalf(mp.mp.dps),tot])
