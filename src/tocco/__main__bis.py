#! /usr/bin/env python3
# coding: utf-8

#  ____  _____  ___  ___  _____
# (_  _)(  _  )/ __)/ __)(  _  )
#   )(   )(_)(( (__( (__  )(_)(
#  (__) (_____)\___)\___)(_____)

# Model of a flow over a topography ToCCo,
# for "Topographic Coupling at Core-Mantle interface". ToCCo is a local model that
# aims to calculate flows over a topography or confined between two boundaries.
# It uses a linear perturbative approach to solve Magneto-Hydro-Dynamic equations.
# It can, therefore, take into consideration: rotation, magnetic field,
# stratification, and fluid viscosity 

import os

__author__ = "Rémy Monville"
__email__ = "remy.monville@univ-grenoble-alpes.fr"

import logging
from . import Pressure_force
from . import Viz_ToCCo
from .mod import *
import pickle
from sympy import *
from sympy import Function, Derivative as D
# from sympy.vector import CoordSys3D, curl, gradient, divergence, Del, laplacian, matrix_to_vector, VectorZero,Vector
import itertools
import time
import matplotlib.pyplot as plt
from sympy.parsing.sympy_parser import parse_expr
import numpy as np
import mpmath as mp
import sys
from rich import print as rprint
from rich.panel import Panel
from rich.text import Text
from rich.progress import track
sys.path.insert(0, "./")  # Search first the params file in current directory

sys.set_int_max_str_digits(10000)
from sympy import init_printing
init_printing() 

os.environ['SYMPY_CACHE_SIZE'] = '1000000'

import configparser
config = configparser.ConfigParser()
config.optionxform=str

try:
    params_filename = sys.argv[1]
    config.read(params_filename)
except:
    raise ValueError('No parameter file given')

mp.mp.dps = config.getint('OPTIONS','precision')
C = CoordSys3D('C')

def curl(Eqq):
    return((diff((Eqq & C.k), y) - diff((Eqq & C.j), z)) * C.i +
           (diff((Eqq & C.i), z) - diff((Eqq & C.k), x)) * C.j +
           (diff((Eqq & C.j), x) - diff((Eqq & C.i), y)) * C.k
           )

def gradient(Eqq):
    return(diff(Eqq, x) * C.i + diff(Eqq, y) * C.j + diff(Eqq, z) * C.k)


def divergence(Eqq):
    return(diff((Eqq&C.i), x) + diff((Eqq&C.j), y) + diff((Eqq&C.k), z))


def get_super(x): 
    normal = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-=()"
    super_s = "ᴬᴮᶜᴰᴱᶠᴳᴴᴵᴶᴷᴸᴹᴺᴼᴾQᴿˢᵀᵁⱽᵂˣʸᶻᵃᵇᶜᵈᵉᶠᵍʰᶦʲᵏˡᵐⁿᵒᵖ۹ʳˢᵗᵘᵛʷˣʸᶻ⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻⁼⁽⁾"
    res = x.maketrans(''.join(normal), ''.join(super_s)) 
    return x.translate(res) 

def mpmathM(A):
    """Convert Sympy Matrix in mpmath Matrix format.

    Attrs:
    - A (Sympy Matrix): Matrix to convert.

    Returns:
    - An mpmath Matrix.
    """
    B = mp.matrix(A.shape[0], A.shape[1])
    for k in range(A.shape[0]):
        for l in range(A.shape[1]):
            B[k, l] = mp.mpc(str(re(A[k, l])), str(im(A[k, l])))
    return(B) 

def mp_pinv_solve(M,r):
    MM = mp.chop(mp.matrix(M))
    U, S, V = mp.svd(MM)
    def inv_non_zero(ss):
        ss = mp.chop(ss,10**(-mp.mp.dps/2))
        if ss == 0:
            ep = 0
        else:
            ep =1/ss
        return(ep)
    E = mp.matrix([inv_non_zero(ss) for ss in S])
    MMp = V.transpose_conj()*mp.diag(E)*U.transpose_conj()
    xx = MMp*mp.matrix(r)


    return(Matrix(xx))


def factorial(n):
    """Compute factorial of n.

    Attrs:
    - int.

    Returns:
    - int.
    """
    if n <= 0:
        return 1
    else:
        return n * factorial(n - 1)


def findexp(Ma):
    """Find all unique exponential forms of Ma (without any simplification of input).
    Attrs:
    - Sympy Matrix.

    Returns:
    - set of Sympy Expression."""

    Ma=list(Ma)
    def coe(x): return list(x.find(exp))
    expo = [coe(ma) for ma in Ma]
    expo = set([val for sublist in expo for val in sublist])
    return(expo)


def makedic(eig, order):
    """Make a dict containing variable of calculus at order 'order' as keys
    and eig as values.

    Attrs:
    - eig: list, array or Matrix of size (1,7) containing sympy Expr.
    - order: int.

    Returns:
    - dict."""
    

    try:
        roro = eig[6]
    except:
        roro = eig[5]*0
    dic = {
        Symbol('u' + str(order) + 'x'): eig[0],
        Symbol('u' + str(order) + 'y'): eig[1],
        Symbol('u' + str(order) + 'z'): eig[2],
        Symbol('b' + str(order) + 'x'): eig[3],
        Symbol('b' + str(order) + 'y'): eig[4],
        Symbol('b' + str(order) + 'z'): eig[5],
        Symbol('rho' + str(order)): roro
    }

    return(dic)

def makedic_mantle(eig, order):
    """Make a dict containing variable of calculus at order 'order' as keys
    and eig as values.

    Attrs:
    - eig: list, array or Matrix of size (1,3) containing sympy Expr.
    - order: int.

    Returns:
    - dict."""
    
    dic = {
        Symbol('bm' + str(order) + 'x'): eig[0],
        Symbol('bm' + str(order) + 'y'): eig[1],
        Symbol('bm' + str(order) + 'z'): eig[2]

    }
    return(dic)

def makedic_IC(eig, order):
    """Make a dict containing variable of calculus at order 'order' as keys
    and eig as values.

    Attrs:
    - eig: list, array or Matrix of size (1,3) containing sympy Expr.
    - order: int.

    Returns:
    - dict. """
    

    dic = {
        Symbol('bm2' + str(order) + 'x'): eig[0],
        Symbol('bm2' + str(order) + 'y'): eig[1],
        Symbol('bm2' + str(order) + 'z'): eig[2]

    }
    return(dic)

def makedic_ins(eig, order):
    """Make a dict containing variable of calculus at order 'order' as keys
    and eig as values.

    Attrs:
    - eig: list, array or Matrix of size (1,3) containing sympy Expr.
    - order: int.

    Returns:
    - dict.
    """
    dic = {
        Symbol('bm_i' + str(order) + 'x'): eig[0],
        Symbol('bm_i' + str(order) + 'y'): eig[1],
        Symbol('bm_i' + str(order) + 'z'): eig[2]

    }
    return(dic)


def strain(U):
    """Return the strain-tensor of velocity field U.

    Attrs:
    - Vector.

    Returns:
    - Sympy Matrix.
    """
    xy = 1 / 2 * (diff(U & C.i, y) + diff(U & C.j, x))
    xz = 1 / 2 * (diff(U & C.i, z) + diff(U & C.k, x))
    yz = 1 / 2 * (diff(U & C.j, z) + diff(U & C.k, y))
    strainT = Matrix([[diff(U & C.i, x), xy, xz],
                      [xy, diff(U & C.j, y), yz],
                      [xz, yz, diff(U & C.k, z)]]).doit()
    return(strainT)


def simp(expr):
    """Best simplification to devellop properly exponential form.

    Attrs:
    - Expr.

    Returns:
    - Expr.
    """
    def simpelem(expr):
        expe = powsimp(expand(expr),combine = 'exp',force=True)
        return(expe)

    if isinstance(expr, Vector) == True:
        expe = (simpelem(expr&C.i)*C.i +
                simpelem(expr&C.j)*C.j +
                simpelem(expr&C.k)*C.k)
    elif isinstance(expr, Matrix) == True or isinstance(expr, ImmutableMatrix) == True:
        expe = expr.applyfunc(simpelem)
    else:
        expe = simpelem(expr)
    return(expe)


def comat(vec, ex):
    """Extract the coefficient of 'ex' exponential form from a Vector 'vec'.

    Attrs:
    - Vector.

    Returns:
    - Vector.
    """
    def coe(x): return x.coeff(ex) * ex
    M = (vec.to_matrix(C))
    M1 = M.applyfunc(coe)
    vec1 = matrix_to_vector(M1, C)
    return(vec1)

def conjv(vect):
    """Return the complex conjugate of a Vector

    Attrs:
    - Vector.

    Returns:
    - Vector.
    """
    ve = matrix_to_vector(((vect).to_matrix(C).H).xreplace({conjugate(x): x, conjugate(
        y): y, conjugate(z): z, conjugate(t): t, conjugate(et): et, conjugate(ev): ev}), C)
    return(ve)


def conj(scal):
    """Return the complex conjugate of an Expr

    Attrs:
    - Expr.

    Returns:
    - Expr.
    """
    ve = conjugate(scal).xreplace({conjugate(x): x, conjugate(y): y, conjugate(
        z): z, conjugate(t): t, conjugate(et): et, conjugate(ev): ev})
    return(ve)


def realve(vect):
    """Return the real part of a Vector.

    Attrs:
    - Vector.

    Returns:
    - Vector.
    """
    ve = (vect + conjv(vect)) / 2
    return(ve)


def null_space(A, ansatz):
    """Return the null space corresponding to the smallest singular value of
    the Matrix A using svd decomposition, solution is verified that it solves
    solenoidal equation. Return Nan in case of failure

    Attrs:
    - A: mpmath Matrix.
    - ansatz: Expr, exponential form associated to A .

    Returns:
    - mpmath Matrix.
    """
    listeigen = []
    A = mpmathM(A)
    u, s, vh = mp.svd_c(A)
    listeigen = []
    ind = -1
    while mp.almosteq(s[len(s)+ind], 0, (10**(-(mp.mp.dps/4)))) == True and len(s)+ind >=0:
        listeigen += [vh[len(s)+ind, :].transpose_conj()]
        ind -=1





    return(listeigen)

def taylor(exp, nv, nt, dic):
    """Taylor expansion in 0, with two variables. Return Taylor coefficient
    of order nv/nt. (you need to multiply output with ev**nt*et**et to have
    the complete Taylor term)

    Attrs:
    - exp: Expr.
    - nv: int, order associated to small parameter ev.
    - nt: int, order associated to small parameter et.
    - dic: dict, dictionary of values to replace in Expr

    Returns:
    - Expr."""

    if nv == 0 and nt == 0:
        expt = exp.xreplace({ev: 0, et: 0})
        return(expt)
    if nt == 0:
        df = diff((exp.xreplace(dic)), ev, nv).xreplace({ev: 0, et: 0})
    elif nv == 0:
        df = diff((exp.xreplace(dic)), et, nt).xreplace({ev: 0, et: 0})
    else:
        df = diff((exp.xreplace(dic)), ev, nv,
                  et, nt).xreplace({ev: 0, et: 0})
    expt = Rational(1 / (factorial(nt) * factorial(nv))) * df
    return expt


def taylor_serie(exp, nvmax, ntmax, dic):
    """Complete Taylor expansion in 0, with two variables.

    Attrs:
    - exp: Expr.
    - nvmax: int, maximum order associated to small parameter ev.
    - ntmax: int, maximum order associated to small parameter et.
    - dic: dict, dictionary of values to replace in Expr

    Returns:
    - Expr.
    """
    nv = np.arange(nvmax + 1)
    nt = np.arange(ntmax + 1)
    expt = 0
    for v in nv:
        for t in nt:
            tay = taylor(exp, v, t, dic) * et**t * ev**v
            try:
                expt += tay
            except:
                pass
    return(expt)

def solve_linear(M,r,test = False):
    M  = mpmathM(M)
    r = mpmathM(r)

    try:
        soluchap = Matrix(mp.lu_solve(M,r))
    except:
        soluchap = mp_pinv_solve(M,r)
        rprint('[yellow]lu_solve not working use pinv')
    if test ==True:
        testee = (Matrix(M)*soluchap-Matrix(r)).evalf(6)

        if abs(sum(testee).evalf()) > 1e-20:
            rprint(f'[bold red]Warning : error  of {sum(abs(testee).evalf(2))}')
            DD = {'M':Matrix(M),'r':Matrix(r)}
            with open('Mtest_prec', 'wb') as outf:
                outf.write(pickle.dumps(DD))
    return(Matrix(soluchap))


def makeEquations(U, B, rho, order_v, order_t, dic):
    """Return the MHD equation set. Some option can be selected in the
    params file:
    - buf : Use the approximation of Buffett(2010)
    - atmo : "remove" magnetic effects
    Attrs:
    - U: Vector, velocity field.
    - B: Vector, magnetic field.
    - p: Expr, pressure field.
    - rho: Expr, density field.
    - order_v: int, order associated to small parameter ev.
    - order_t: int, order associated to small parameter et.
    - dic: dict, dictionary of values to replace in Expr

    Returns:
    - Matrix containing [Momentum_x,
                        Momentum_y,
                        Vorticity_z,
                        Momentum_z,
                        Induction_x,
                        Induction_y,
                        Induction_z,
                        divU = 0,
                        Mass conservation].

    """
    Eq_rho = diff(rho, t) + U.dot(gradient(rho)) + ((U&C.k)*qFr**2) - qRer*laplacian(rho)
    Cor = (2 * qRo * fC.xreplace(DIC)).cross(U)

    UgradU  = AgradB(U,U)
    JB = curl(B).cross(B)
    if buf == 1:
        UgradU = 1e-10 * C.i
    if atmo == 1 or buf ==1:
        JB = 1e-10 * JB
    if eval(config.get('BOUNDARY.CONDITIONS','U')) == 'inviscid':
        Eq_NS = diff(U, t) + Cor +UgradU - (  (rho*C.k) + JB)
    else:
        Eq_NS = diff(U, t) + Cor  +UgradU - (
                                             qRe * laplacian(U) + (rho*C.k) + JB)

    Eq_vort = curl(Eq_NS)
    Eq_m = divergence(U)


    if buf == 1:
        Eq_b = diff(B, t) - (qRm * diff(B, z, z)) - qAl * diff(U, z)
    if atmo == 1:
        Eq_b = 0*C.i
        Eq_divB = sympify(0)
    else:
        Eq_b = diff(B, t) - (qRm * laplacian(B) + curl(U.cross(B)))
        Eq_divB = divergence(B)
    eq = zeros(9, 1)
    for ii, j in enumerate([Eq_m, Eq_vort & C.i,Eq_vort & C.j, Eq_vort & C.k, Eq_divB, Eq_b & C.i, Eq_b & C.j, Eq_b & C.k, Eq_rho]):
        if order_v == 'no':
            eq[ii] = j

        else:
            eq[ii] = taylor(j, order_v, order_t, dic)
    eq = Matrix([simp(eqx.xreplace(dic)) for eqx in eq])

    return(eq)

def makeEquations_fast(U, B, rho, order_v, order_t, dic):
    """Return the MHD equation set. Some option can be selected in the
    params file:
    - buf : Use the approximation of Buffett(2010)
    - atmo : "remove" magnetic effects

    Attrs:
    - U: Vector, velocity field.
    - B: Vector, magnetic field.
    - p: Expr, pressure field.
    - rho: Expr, density field.
    - order_v: int, order associated to small parameter ev.
    - order_t: int, order associated to small parameter et.
    - dic: dict, dictionary of values to replace in Expr

    Returns:
    - Matrix containing [Momentum_x,
                        Momentum_y,
                        Vorticity_z,
                        Momentum_z,
                        Induction_x,
                        Induction_y,
                        Induction_z,
                        divU = 0,
                        Mass conservation].
    """
    U,Uq = U
    B,Bq = B
    rho,rhoq = rho


    NL_U_rho = taylor(Uq.dot(gradient(rhoq)), order_v, order_t, dic)

    JB  = curl(Bq).cross(Bq)
    JB = taylor(JB, order_v, order_t, dic)
    UgradU  = AgradB(Uq,Uq)
    UgradU = taylor(UgradU, order_v, order_t, dic)

    UcrossB = taylor(curl(Uq.cross(Bq)), order_v, order_t, dic)

    Eq_m = divergence(U)
    Eq_divB = divergence(B)
    Cor = (2 * qRo * fC.xreplace(DIC)).cross(U)

    Eq_rho = diff(rho, t) + NL_U_rho + ((U&C.k)*qFr**2) - qRer*laplacian(rho)

    if buf == 1:
        JB = 1e-10 * JB
        UgradU = 1e-10 * C.i
        Eq_b = diff(B, t) - (qRm * diff(B, z, z)) - qAl * diff(U, z)
    if atmo == 1:
        JB = 0 * JB
        #Eq_b = diff(B, t) - (qRm * laplacian(B) + mp.mpf('1e-10') * UcrossB)
        Eq_b = 0*C.i
        Eq_divB = 0
    else:
        Eq_b = diff(B, t) - (qRm * laplacian(B) + UcrossB)

    if eval(config.get('BOUNDARY.CONDITIONS','U')) == 'inviscid':
        Eq_NS = diff(U, t) + Cor +UgradU - ( (rho*C.k) + JB)
    else:
        Eq_NS = diff(U, t) + Cor +UgradU - (qRe * laplacian(U) + (rho*C.k) + JB)
    Eq_vort = curl(Eq_NS)
    eq = zeros(9, 1)
    for ii, j in enumerate([Eq_m, Eq_vort & C.i, Eq_vort & C.j, Eq_vort & C.k, Eq_divB,Eq_b & C.i, Eq_b & C.j, Eq_b & C.k, Eq_rho]):
        eq[ii] = j

    eq = Matrix([simp(eqx.xreplace(dic)) for eqx in eq])

    return(eq)

def makeEquations_fast_rmep(U, B, rho, order_v, order_t, dic):
    U,Uq = U
    B,Bq = B
    rho,rhoq = rho
    NL_U_rho = taylor(Uq.dot(gradient(rhoq)), order_v, order_t, dic)
    JB  = curl(Bq).cross(Bq)
    UgradU  = AgradB(Uq,Uq)

    JB = taylor(JB, order_v, order_t, dic)
    UgradU = taylor(UgradU, order_v, order_t, dic)
    UcrossB = taylor(curl(Uq.cross(Bq)), order_v, order_t, dic)

    Eq_m = 0
    Eq_divB = 0
    Eq_rho = NL_U_rho
    if buf == 1:
        JB = 1e-10 * JB
        UgradU = 1e-10 * C.i
        Eq_b = 0*C.i
    if atmo == 1:
        JB = 0 * JB
        Eq_b =  0*C.i
    else:
        Eq_b = - UcrossB

    Eq_NS = UgradU - JB


    Eq_vort = curl(Eq_NS)

    eq = zeros(9, 1)
    for ii, j in enumerate([Eq_m, Eq_vort & C.i, Eq_vort & C.j, Eq_vort & C.k, Eq_divB,Eq_b & C.i, Eq_b & C.j, Eq_b & C.k, Eq_rho]):
        eq[ii] = j
    eq = Matrix([simp(eqx.xreplace(dic)) for eqx in eq])


    return(eq)


def eigen(M, ansatz, dic,top =False):
    """Find unknown parameter kz in Matrix M as det(M(kz)) = 0, Then return the
    nullspace A of the corresponding Matrix using null_space as M(kz)·A = 0

    Attrs:
    - M: Matrix.
    - ansatz: Expr, exponential form associated to M.
    - dic: dict, dictionary of values to replace in Expr

    Returns:
    - solr1: array, contains kzs as Expr.
    - eigen1: array, contains nullspaces as mpmath Matrix.
    - M1: Matrix, Matrix modified by dic
    """

    if M.shape[0]!= M.shape[1]:
        rprint('[yellow] USE ECHELON FORM ! non square matrix')

        redM = reduced_M(M)

        determinant= (redM).det(method='berkowitz')
    else:
        determinant = (M).det(method='berkowitz')

    if determinant ==0:
        rprint('[yellow] USE ECHELON FORM det =0!')
        determinant= (reduced_M(M)).det(method='berkowitz')
    detp  = Poly(determinant,kz).monic()
    co = detp.all_coeffs()
    # try:
    while co[0] == 0:
        co = co[1:]
    while co[-1] == 0:
        co = co[:-1]

    maxsteps = 3000
    extraprec = 5000
    co = mpmathM(Matrix(co))
    if len(co) ==1:
        solr = np.array([0])
    else:
        sol, err = mp.polyroots(
            co, maxsteps=maxsteps, extraprec=extraprec, error=True)
        sol = np.array(sol)
        solr = sol
        solr = solr[[mp.fabs(m) < 10**(10) for m in solr]]
        solr = np.array([mp.chop(m,10**-(mp.mp.dps*0.9)) for m in solr],dtype= object)
        if Bound_nb == 1:
            if top ==True:
                solr = solr[[mp.im(m) > 0  for m in solr]]
            else:
                solr = solr[[mp.im(m) < 0  for m in solr]]

    if len(solr) == 0:
        solr = np.array([0])
    eigen1 = []
    solr1 = []
    
    for solkz in solr:
        M2 = simp(M.xreplace({kz: solkz}))
        global solrnow
        solrnow = solkz
        eigen_now= null_space(M2, ansatz)
        for eigi in eigen_now:
            eigen1 += [eigi]
            solr1 += [solkz]

    eigen1 = np.array(eigen1,dtype=object)
    solr1 = np.array(solr1,dtype=object)

    rprint(f"[green bold]Number of solutions : {str(len(solr))} found ")
    with mp.workdps(5):
        rprint('Kz      :      ', end = "")
        pprint(Matrix(sympify(solr1)).T)
        print("\n")
    return(solr1, eigen1, M)


def veigen(eig, sol, ansatz):
    """Write variable as function of found coefficients

    Attrs:
    - eig: mpmath Matrix, found nullspace from eigen().
    - sol: Expr, found kz from eigen().
    - ansatz: Expr, exponential form associated.

    Returns:
    - Matrix, containing [U_x, U_y, U_z, B_x, B_y, B_z, rho]
    """
    veig = 0
    for s in range(len(sol)):
        veig = veig + Symbol('C' + str(s)) * \
            eig[s] * ansatz.xreplace({kz: sol[s]})
    return(veig)

def veigen_mantle(eig, sol, ansatz):
    """Write variable as function of found coefficients

    Attrs:
    - eig: mpmath Matrix, found nullspace from eigen().
    - sol: Expr, found kz from eigen().
    - ansatz: Expr, exponential form associated.

    Returns:
    - Matrix, containing [U_x, U_y, U_z, B_x, B_y, B_z, rho]
    """
    veig = 0
    for s in range(len(sol)):
        veig = veig + Symbol('Cm' + str(s)) * \
            eig[s] * ansatz.xreplace({kz: sol[s]})
    return(veig)

def veigen_IC(eig, sol, ansatz):
    """Write variable as function of found coefficients

    Attrs:
    - eig: mpmath Matrix, found nullspace from eigen().
    - sol: Expr, found kz from eigen().
    - ansatz: Expr, exponential form associated.

    Returns:
    - Matrix, containing [U_x, U_y, U_z, B_x, B_y, B_z, rho]
    """
    veig = 0
    for s in range(len(sol)):
        veig = veig + Symbol('Cc' + str(s)) * \
            eig[s] * ansatz.xreplace({kz: sol[s]})
    return(veig)

def veigen_ins(eig, sol, ansatz):
    """Write variable as function of found coefficients

    Attrs:
    - eig: mpmath Matrix, found nullspace from eigen().
    - sol: Expr, found kz from eigen().
    - ansatz: Expr, exponential form associated.

    Returns:
    - Matrix, containing [U_x, U_y, U_z, B_x, B_y, B_z, rho]
    """
    veig = 0
    for s in range(len(sol)):
        veig = veig + Symbol('C_ins' + str(s)) * \
            eig[s] * ansatz.xreplace({kz: sol[s]})
    return(veig)



def surfcond(val, dic):
    """Return equation at the 1rst boundary

    Attrs:
    - val: Expr.
    - dic: dict, dictionary of values to replace in Expr, must contain the
    topography eq as f and f0.

    Returns:
    -Expr.
    """

    va = val.xreplace(dic).doit().xreplace({
        f0.xreplace(dic): -(f - f0)}).xreplace(dic)
    return(va)


def surfcond_2(val, dic):
    """Return equation at the 2nd boundary

    Attrs:
    - val: Expr.
    - dic: dict, dictionary of values to replace in Expr, must contain the
    topography eq as f_2 and f0_2.

    Returns:
    -Expr.
    """
    va = val.xreplace(dic).doit().xreplace(
        {f0_2.xreplace(dic): -(f_2 - f0_2)}).xreplace(dic)
    return(va)


def Bound_nosolve(U, B,rho, psi, psi_2b, dic, order_v, order_t):
    """Return boundary conditions equations. Some option can be selected in the
    params file:
    - condB : magnetic condition
    - condU : velocity condition

    Attrs:
    - U: Vector, velocity field.
    - B: Vector, magnetic field.
    - psi: - Vector, magnetic field in the mantle, if conducting,
           - Expr, Scalar potential of the magnetic field, if insulating.
    - psi_2b: same as psi in the 2nd boundary.
    - dic: dict, dictionary of values to replace in Expr
    - order_v: int, order associated to small parameter ev.
    - order_t: int, order associated to small parameter et.


    Returns:
    - Matrix, containing BC varying with parameters.
    """

    nn = n
    if Bound_nb ==2:
        nn2 = n2

    # B_o = taylor(B, order_v, order_t, dic)
    condB = eval(config.get('BOUNDARY.CONDITIONS','B'))
    condU = eval(config.get('BOUNDARY.CONDITIONS','U'))
    BC_list = []

    #Conditions TOP
    
    
    if condU[0] == 'stressfree':
        #Uparoi = diff(-f,t)*C.k
        un = U.dot(nn)#-(Uparoi).dot(nn)
        eu = matrix_to_vector(strain(U) * nn.to_matrix(C),C).cross(nn) #-strain(Uparoi) * nn.to_matrix(C)
        eu1 = eu&C.i
        eu2 = eu&C.j
        Eq_n1 = surfcond(un, dic).xreplace(dic)
        Eq_n2 = surfcond(eu1, dic)
        Eq_n3 = surfcond(eu2, dic)
        BC_list += [Eq_n1,Eq_n2,Eq_n3]
    elif condU[0] == 'inviscid':
        Uparoi = diff(-f,t)*C.k
        un = U.dot(nn)-(Uparoi).dot(nn)
        Eq_n1 = surfcond(un, dic).xreplace(dic)
        BC_list += [Eq_n1]
    elif condU[0] == 'noslip':
        Uparoi = diff(-f,t)*C.k
        un = U.dot(nn)#-(Uparoi).dot(nn)
        ut = U.cross(nn)#-(Uparoi).cross(nn)
        Eq_n1 = surfcond(un, dic).xreplace(dic)
        Eq_n2 = surfcond(ut&C.i, dic).xreplace(dic)
        Eq_n3 = surfcond(ut&C.j, dic).xreplace(dic)
        BC_list += [Eq_n1,Eq_n2,Eq_n3]
    else:
        raise NameError('Error non valid top BC for U')

    if config.getboolean('OPTIONS','atmo') == False:
        if condB[0] == "insulator":
            bb = B - psi
            Eq_b = surfcond(bb, dic)
            Eq_bx = Eq_b & C.i
            Eq_by = Eq_b & C.j
            Eq_bz = Eq_b & C.k
            BC_list += [Eq_bx, Eq_by, Eq_bz]
        elif condB[0] == "conductor":
            conservation_B = surfcond(B - psi, dic)
            conservation_E = nn.cross(qRm * curl(B) - qRmm * curl(psi) - (U.cross(B)))
            consE = surfcond(conservation_E, dic)
            Eq_Ex = consE & C.i
            Eq_Ey = consE & C.j
            Eq_bx = conservation_B & C.i
            Eq_by = conservation_B & C.j
            Eq_bz = conservation_B & C.k
            BC_list += [Eq_bx, Eq_by, Eq_bz, Eq_Ex, Eq_Ey]
        else:
            raise NameError('Error non valid top BC for B')

    if qRer.xreplace(DIC) != 0:
            Eq = surfcond(gradient(rho).dot(nn), dic)
            BC_list += [Eq]


    #Conditions Bottom
    
    if config.getint('OPTIONS','Bound_nb') == 2:
        if condU[1] == 'stressfree':
            #Uparoi = diff(-f,t)*C.k
            un = U.dot(nn2)#-(Uparoi).dot(nn)
            eu = matrix_to_vector(strain(U) * nn2.to_matrix(C),C).cross(nn2) #-strain(Uparoi) * nn.to_matrix(C)
            eu1 = eu&C.i
            eu2 = eu&C.j
            Eq_n1 = surfcond_2(un, dic).xreplace(dic)
            Eq_n2 = surfcond_2(eu1, dic)
            Eq_n3 = surfcond_2(eu2, dic)
            BC_list += [Eq_n1,Eq_n2,Eq_n3]
        elif condU[1] == 'inviscid':
            Uparoi = diff(-f_2,t)*C.k
            un = U.dot(nn2)-(Uparoi).dot(nn2)
            Eq_n1 = surfcond_2(un, dic).xreplace(dic)
            BC_list += [Eq_n1]
        elif condU[1] == 'noslip':
            Uparoi = diff(-f_2,t)*C.k
            Uparoi = 0
            un = U.dot(nn2)#-(Uparoi).dot(nn2)
            ut = U.cross(nn2)#-(Uparoi).cross(nn2)
            Eq_n1 = surfcond_2(un, dic).xreplace(dic)
            Eq_n2 = surfcond_2(ut&C.i, dic).xreplace(dic)
            Eq_n3 = surfcond_2(ut&C.j, dic).xreplace(dic)
            BC_list += [Eq_n1,Eq_n2,Eq_n3]
        else:
            raise NameError('Error non valid bottom BC for U')

        if config.getboolean('OPTIONS','atmo') == False:
            if condB[1] == "insulator":
                bb = B - psi_2b
                Eq_b = surfcond_2(bb, dic)
                Eq_bx = Eq_b & C.i
                Eq_by = Eq_b & C.j
                Eq_bz = Eq_b & C.k
                BC_list += [Eq_bx, Eq_by, Eq_bz]
            elif condB[1] == "conductor":
                conservation_B = surfcond_2(B - psi_2b, dic)
                if buf == 1:
                    conservation_E = qRm * diff(B, z) - qRmc * diff(psi_2b, z) + qAl * U
                else:
                    conservation_E = nn2.cross(
                    qRm * curl(B) - qRmc * curl(psi_2b) - (U.cross(B)))

                consE = surfcond_2(conservation_E, dic)
                Eq_Ex = consE & C.i
                Eq_Ey = consE & C.j
                Eq_bx = conservation_B & C.i
                Eq_by = conservation_B & C.j
                Eq_bz = conservation_B & C.k
                BC_list += [Eq_bx, Eq_by, Eq_bz, Eq_Ex, Eq_Ey]
            else:
                raise NameError('Error non valid bottom BC for B')

        if qRer.xreplace(DIC) != 0:
            Eq = surfcond_2(gradient(rho).dot(nn2), dic)
            BC_list += [Eq]

    if order_v == 'no':
        TEq = BC_list
    else:
        def tayBC(eq):
            tay = taylor(eq, order_v, order_t, dic)
            return(tay)
        TEq = [tayBC(i) for i in BC_list]
    TEq = Matrix([simp(sympify(expand(tex)).xreplace(dic)) for tex in TEq])
    return(simp(TEq))

def intepi(requ, zeta_t):
    """Integrate on 1 wavelength at y=0.

    Attrs:
    - requ: Expr.
    - zeta_t: float, chosen non-dimensional topography height.

    Returns:
    -Expr.
    """
    zeta_t = np.float64(zeta_t)
    func = abs(re(requ.xreplace({y: 0, et: zeta_t, ev: 1})))
    free = lambdify([x], func)
    integrale = integ.quad(lambda xe: free(xe), -np.pi, np.pi)[0]
    return(integrale / (2 * np.pi))

def reduced_M(M):
    M = nsimplify(M)
    M = M.echelon_form()
    m, n = M.shape
    rows = [i for i in range(m) if any(M[i, j] != 0 for j in range(n))]
    cols = [j for j in range(n) if any(M[i, j] != 0 for i in range(m))]
    M = M[rows, cols]
    return(M)


def BaseField(dic):
    """Return expression of base fields. Compute the pressure field

    Attrs:
    - dic: dict, dictionary containing all base fields information:
        -u0x,u0y,u0z: velocity field,
        -b0x,b0y,b0z: magnetic field,
        -rho0: density profile,
        -qAl: 1/Alfven number

    Returns:
    -Expr: U, B, p, rho, psi, psi_2b.
    """
    U = u0.xreplace(DIC)
    B = qAl * b0.xreplace(DIC)
    U =U.xreplace(DIC)
    B = B.xreplace(DIC)
    p = sympify(0)
    rho = sympify(0)
    psi= B
    psi_2b= B
    a,b,c,d,e,f,g,h,i,j,k,l = symbols('a,b,c,d,e,f,g,h,i,j,k,l')

    psi =  B + a*z*C.i+b*z*C.j+c*z*C.k
    psi_2b = B + d*z*C.i+e*z*C.j+f*z*C.k
    eqpsi0 = Bound_nosolve(U, B,rho, psi, psi_2b,DIC, 0, 0)

    eqpsi0 = eqpsi0.row_insert(-1, Matrix([[divergence(psi)]]))
    eqpsi0 = eqpsi0.row_insert(-1, Matrix([[divergence(psi_2b)]]))
    #eqpsi0 = eqpsi0.row_insert(-1, Matrix([[divergence(U)]]))
    sops = solve(eqpsi0,[a,b,c,d,e,f,g,h,i,j,k,l],rational= False)
    if sops != []:
        psi = psi.xreplace(sops).xreplace({a:0,b:0,c:0,d:0,e:0,f:0,g:0,h:0,i:0,j:0,k:0,l:0})
        psi_2b = psi_2b.xreplace(sops).xreplace({a:0,b:0,c:0,d:0,e:0,f:0,g:0,h:0,i:0,j:0,k:0,l:0})
        U = U.xreplace(sops).xreplace({a:0,b:0,c:0,d:0,e:0,f:0,g:0,h:0,i:0,j:0,k:0,l:0})
    else:
        psi = psi.xreplace({a:0,b:0,c:0,d:0,e:0,f:0,g:0,h:0,i:0,j:0,k:0,l:0})
        psi_2b = psi_2b.xreplace({a:0,b:0,c:0,d:0,e:0,f:0,g:0,h:0,i:0,j:0,k:0,l:0})
        U = U.xreplace({a:0,b:0,c:0,d:0,e:0,f:0,g:0,h:0,i:0,j:0,k:0,l:0})
    bou = Bound_nosolve(U, B,rho, psi, psi_2b, DIC, 0,0)

    push =False
    if not mp.almosteq(sum(abs(bou)).evalf(),0,10**(-mp.mp.dps/2)):
        push =True
        rprint('[yellow] Order 0 false ad non homogenous field')

        U *=ev

    psi =  B + a*z*C.i+b*z*C.j+c*z*C.k
    psi_2b = B + d*z*C.i+e*z*C.j+f*z*C.k

    eqpsi0 = Bound_nosolve(U, B,rho, psi, psi_2b,DIC, 0, 0)
    eqpsi0 = eqpsi0.row_insert(-1, Matrix([[divergence(psi)]]))
    eqpsi0 = eqpsi0.row_insert(-1, Matrix([[divergence(psi_2b)]]))
    sops = solve(eqpsi0,[a,b,c,d,e,f],rational= False)
    psi = psi.xreplace(sops).xreplace({a:0,b:0,c:0,d:0,e:0,f:0})
    psi_2b = psi_2b.xreplace(sops).xreplace({a:0,b:0,c:0,d:0,e:0,f:0})

    if push ==True:
        psi =  psi + ev*(a*z*C.i+b*z*C.j+c*z*C.k )
        psi_2b = psi_2b + ev*(d*z*C.i+e*C.i)

        eqpsi0 = Bound_nosolve(U, B,rho, psi, psi_2b,DIC, 1, 0)
        MM,r = linear_eq_to_matrix(eqpsi0,[a,b,c,d,e,f])
        so = list(solve_linear(MM,r))
        psi = psi.xreplace(dict(zip([a,b,c,d,e,f],so)))
        psi_2b = psi_2b.xreplace(dict(zip([a,b,c,d,e,f],so)))

    pprint(Eq(Symbol('u_0'),U.to_matrix(C).T,evaluate=False))
    pprint(Eq(Symbol('b_0'),Eq(Symbol('psi'),B.to_matrix(C).T,evaluate=False),evaluate=False))
    return(U, B, p, rho, psi, psi_2b)


def compute_order(U, B, p, rho, psi, psi_2b, dic, iv, it):
    """Return computed field at given order

    Attrs:
    - U: Vector, velocity field.
    - B: Vector, magnetic field.
    - p: Expr, pressure field.
    - rho: Expr, density field.
    - psi: - Vector, magnetic field in the mantle, if conducting,
           - Expr, Scalar potential of the magnetic field, if insulating.
    - psi_2b: same as psi in the 2nd boundary.
    - dic: dict, dictionary containing all base fields information:
    - iv: int, order associated to small parameter ev.
    - it: int, order associated to small parameter et.

    Returns:
    -Expr: U, B, p, rho, psi, psi_2b.
    """
    i = iv+it

    rprint(Panel(Text(f"||| ORDER εₜ{get_super(str(it))} εᵥ{get_super(str(iv))} |||", justify="center")))
    so_part = zeros(7, 1)

    if iv == 10 and it == 0:
        """ solve only b for perturbation of U"""
        rprint("first step")
        TEq = Bound_nosolve(U, B,rho, psi, psi_2b, DIC, iv, it)
        expo = list(findexp(TEq))
        B0 = B
        psi0 = psi

        if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == "conductor":
            for ex in expo:
                ansatz = ex * exp(I * kz * z)
                B0 += (Symbol('BOx') * C.i + Symbol('BOy') *
                       C.j + Symbol('BOz') * C.k) * ev * ansatz
                psi0 += (Symbol('bmx') * C.i + Symbol('bmy') *
                         C.j + Symbol('bmz') * C.k) * ev * ansatz

            eqpsi0 = diff(psi0, t) - (qRmm * laplacian(psi0))

            eqb0 = diff(B0, t) - (qRm * laplacian(B0) + (curl(U.cross(B0))))

            eqb0 = simp(taylor(eqb0, 1, 0, DIC))
            eqpsi0 = simp(taylor(eqpsi0, 1, 0, DIC))
            exp_time = findexp(Matrix([eqpsi0]))
            psi1 = psi
            B1 = B
            for ext in exp_time:
                def coe(x): return x.coeff(ext)
                equ = (simplify(comat(eqpsi0, ext) / ext)).to_matrix(C)
                sokzpsi = solve(equ[0].xreplace({y:0,z:0}), kz, rational=False)
                sokzpsi = np.array([m.evalf(mp.mp.dps) for m in sokzpsi])
                sokzpsi = sokzpsi[[mp.im(m.xreplace(DIC).evalf(mp.mp.dps)) > 10**(
                    -mp.mp.dps) for m in sokzpsi]][0].xreplace(DIC).evalf(mp.mp.dps)
                psi1 = psi1 + (Symbol('bmx') * C.i + Symbol('bmy')
                               * C.j) * ext.xreplace({kz: sokzpsi}) * ev
                equ = (simplify(comat(eqb0, ext) / ext)).to_matrix(C)
                sokzb = solve(equ[0].xreplace({y:0,z:0}), kz)
                sokzb = np.array([m.evalf(mp.mp.dps) for m in sokzb])
                sokzb = sokzb[[mp.im(m.xreplace(DIC).evalf(mp.mp.dps)) < -10**(-mp.mp.dps)
                               for m in sokzb]][0].xreplace(DIC).evalf(mp.mp.dps)
                B1 = B1 + (Symbol('BOx') * C.i + Symbol('BOy') *
                           C.j) * ext.xreplace({kz: sokzb}) * ev

            psi_2b = 1
            Boun0 = Bound_nosolve(U, B1,rho, psi1, psi_2b, {
                                  **DIC, **DIC}, 1, 0)
            exp_time = findexp(Matrix([B1.to_matrix(C), psi1.to_matrix(C)]))
            for ext in exp_time:
                def coe(x): return x.coeff(ext.xreplace({y:0,z: 0}))
                soB0 = solve(Boun0.applyfunc(coe).xreplace({y:0,z: 0}),
                             dict=True, rational=False)[0]
                B += comat(B1, ext).xreplace(soB0)
                psi += comat(psi1, ext).xreplace(soB0)

    elif i >= 0:
        rprint(Panel(Text(f"||| PARTICULAR SOLUTION |||", justify="center"),border_style = 'blue'))

        eq  = makeEquations_fast([0*C.i,U], [0*C.i,B], [sympify(0),rho], iv, it, DIC)
        var = [Symbol('u' + str(i) + 'x'), Symbol('u' + str(i) + 'y'), Symbol('u' + str(i) + 'z'),
               Symbol('b' + str(i) + 'x'), Symbol('b' + str(i) + 'y'), Symbol('b' + str(i) + 'z'), Symbol('rho' + str(i))]

        Mc, rmec = linear_eq_to_matrix(eq, var)

        expo = findexp(rmec)
        so_part = zeros(7, 1)

        Up = 0*C.i
        Bp = 0*C.i
        rhop = sympify(0)

        varom = [Symbol('u' + str(i) + 'x'), Symbol('u' + str(i) + 'y'), Symbol('u' + str(i) + 'z'),
                 Symbol('b' + str(i) + 'x'), Symbol('b' + str(i) + 'y'), Symbol('b' + str(i) + 'z'), Symbol('rho' + str(i))]

        var_keep = varom
        for ansatz in expo:
            Up += (Symbol('u' + str(i) + 'x') * C.i + Symbol(
                'u' + str(i) + 'y') * C.j + Symbol('u' + str(i) + 'z') * C.k) * ansatz
            Bp += (Symbol('b' + str(i) + 'x') * C.i + Symbol(
                'b' + str(i) + 'y') * C.j + Symbol('b' + str(i) + 'z') * C.k) * ansatz
            rhop += (Symbol('rho' + str(i))) * ansatz

        eqt = makeEquations(U+ev**iv * et**it *Up,B+ev**iv * et**it *Bp, rho+ev**iv * et**it *rhop, iv, it, DIC)
        eqttot = eqt

        expo = findexp(Matrix(eqt))

        for ansatz in expo:
            rprint("**   ",pretty(ansatz.args[0].evalf(5).xreplace(dicprint)))
            var = var_keep
            def coe(x): return x.coeff(ansatz)
            eqp = eqt.applyfunc(coe)
            eqttot -= eqp*ansatz

            if config.getboolean('OPTIONS','test') == 1:
                eqttot = simp(eqttot)

            ansatz0= ansatz.xreplace({z:0})
            tilt = (simplify((gradient(ansatz0) / ansatz0)
                             ).normalize()).to_matrix(C)
            tiltu = u0.xreplace(dic).to_matrix(C)

            if tilt[0] != 0 and tilt[1] == 0 :
                eqp.row_del(1)
                eqp.row_del(4)
            elif tilt[0] == 0 and tilt[1] != 0 :
                eqp.row_del(2)
                eqp.row_del(5)
            elif tilt[0] == 0 and tilt[1] == 0:
                eqp.row_del(3)
                eqp.row_del(6)
            else:
                eqp.row_del(1)
                eqp.row_del(4)

            M, r = linear_eq_to_matrix(eqp,varom)
            M = M.xreplace({y:0,z:0})
            soluchap = solve_linear(M,r,test = True)
            sop = soluchap * ansatz
            varom= var_keep
            so_part += sop
        if config.getboolean('OPTIONS','test') == 1:
            eqttot = simp(simplify(eqttot))

    #####################################
    ######  Homogeneous solution  #######
    #####################################
    if (iv, it) != (0, 0) :
        rprint(Panel(Text(f"||| HOMOGENEOUS SOLUTION |||", justify="center"),border_style = 'blue'))

        # Create the variable as Ubnd = Sum(U,0,n-1) + Upart + U hom
        Usopart = so_part[0] * C.i + \
            so_part[1] * C.j + so_part[2] * C.k
        Bsopart = so_part[3] * C.i + \
            so_part[4] * C.j + so_part[5] * C.k
        rhosopart = so_part[6]

        ### test of res of particular sol ###
        if config.getboolean('OPTIONS','test') == 1:
            rprint('Test of the particular solutions at order E', (iv, it))
            testequations = makeEquations(
                U + ev**iv * et**it * Usopart, B + ev**iv * et**it * Bsopart, rho + ev**iv * et**it * rhosopart, iv, it, DIC)
            testequations = (testequations.xreplace(
                {x: 1, y: -3.2, z: -1, t: 10})).evalf()
            testequations = testequations.evalf()
            rprint('Residuals = ')
            rprint(testequations)

        TEq = Bound_nosolve(U+ ev**iv * et**it*Usopart, B+ ev**iv * et**it*Bsopart,rho+ ev**iv * et**it*rhosopart, psi, psi_2b,DIC, iv, it)
        expo = list(findexp(Matrix([TEq])))
        ee = simp(expand(sum(TEq)))
        aa =ee
        for l  in expo:
            aa = aa - ee.coeff(l)*l

        if simplify(aa) != 0:
            expo += [1]

        Uh = 0*C.i
        Bh = 0*C.i
        rhoh = 0
        psih = 0*C.i
        if Bound_nb == 2:
            psi2h = 0*C.i
        if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == 'layer':
            psi_insu =0*C.i

        for ex in expo:
            ansatz = ex * exp(I * kz * z)
            Uh = Uh + (Symbol('u' + str(i) + 'x') * C.i + Symbol(
                'u' + str(i) + 'y') * C.j + Symbol('u' + str(i) + 'z') * C.k) * ansatz
            Bh = Bh + (Symbol('b' + str(i) + 'x') * C.i + Symbol(
                'b' + str(i) + 'y') * C.j + Symbol('b' + str(i) + 'z') * C.k) * ansatz
            rhoh = rhoh + (Symbol('rho' + str(i))) * ansatz
            psih = psih + ev**iv * et**it * ((Symbol('bm' + str(i) + 'x') * C.i + Symbol('bm' + str(i) + 'y') * C.j + Symbol('bm' + str(i) + 'z') * C.k) * ansatz)
            if Bound_nb == 2:
                psi2h = psi2h + ev**iv * et**it * ((Symbol('bm2' + str(i) + 'x') * C.i + Symbol('bm2' + str(i) + 'y') * C.j + Symbol('bm2' + str(i) + 'z') * C.k) * ansatz)
            if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == 'layer':
                psi_insu =  psi_insu + ev**iv * et**it * ((Symbol('bm_i' + str(i) + 'x') * C.i + Symbol('bm_i' + str(i) + 'y') * C.j + Symbol('bm_i' + str(i) + 'z') * C.k) * ansatz)

        # Calculate the governings equations
        # eqh = makeEquations_fast([Uh,U+ev**iv * et**it *Uh], [Bh,B+ev**iv * et**it *Bh], [rhoh,rho+ev**iv * et**it *rhoh], iv, it, DIC)
        eqh = makeEquations(U+ev**iv * et**it *Uh,B+ev**iv * et**it *Bh, rho+ev**iv * et**it *rhoh, iv, it, DIC)

        var = [Symbol('u' + str(i) + 'x'), Symbol('u' + str(i) + 'y'), Symbol('u' + str(i) + 'z'),
               Symbol('b' + str(i) + 'x'), Symbol('b' + str(i) + 'y'), Symbol('b' + str(i) + 'z'), Symbol('rho' + str(i))]

        Mtot,rtot = linear_eq_to_matrix(eqh,var)
        expeq = findexp(Mtot)

        # Calculate governing equation in the top boundary
        divpsi = simp(taylor(divergence(psih),iv,it,DIC))
        if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == "conductor" or eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == "layer":
            varp = [Symbol('bm' + str(i) + 'x'),Symbol('bm' + str(i) + 'y'),Symbol('bm' + str(i) + 'z')]
            eqps = simp(taylor(diff(psih, t) - qRmm * laplacian(psih),iv,it,DIC))
            Mpsi,Rpsi = linear_eq_to_matrix([eqps&C.i,eqps&C.j,eqps&C.k,divpsi],varp)

        if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == "layer":
            varp = [Symbol('bm_i' + str(i) + 'x'),Symbol('bm_i' + str(i) + 'y'),Symbol('bm_i' + str(i) + 'z')]
            eqps = simp(taylor(curl(psi_insu),iv,it,DIC))
            Mpsi2,Rpsi2 = linear_eq_to_matrix([eqps&C.i,eqps&C.j,eqps&C.k,divpsi],varp)

        if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == "insulator":
            varp = [Symbol('bm' + str(i) + 'x'),Symbol('bm' + str(i) + 'y'),Symbol('bm' + str(i) + 'z')]
            eqps = simp(taylor(curl(psih),iv,it,DIC))
            Mpsi,Rpsi = linear_eq_to_matrix([eqps&C.i,eqps&C.j,eqps&C.k,divpsi],varp)

        # Calculate governing equation in the bottom boundary
        if Bound_nb == 2:
            divpsi2 = simp(taylor(divergence(psi2h),iv,it,DIC))
            if eval(config.get('BOUNDARY.CONDITIONS','B'))[1] == "conductor":
                varp = [Symbol('bm2' + str(i) + 'x'),Symbol('bm2' + str(i) + 'y'),Symbol('bm2' + str(i) + 'z')]
                eqps = simp(taylor(diff(psi2h, t) - qRmc * laplacian(psi2h),iv,it,DIC))
                Mpsi2,Rpsi2 = linear_eq_to_matrix([eqps&C.i,eqps&C.j,eqps&C.k,divpsi2],varp)

            if eval(config.get('BOUNDARY.CONDITIONS','B'))[1] == "insulator":
                # varp = [Symbol('bm2')]
                varp = [Symbol('bm2' + str(i) + 'x'),Symbol('bm2' + str(i) + 'y'),Symbol('bm2' + str(i) + 'z')]
                eqps2 = simp(taylor(curl(psi2h),iv,it,DIC))
                Mpsi2,Rpsi2 = linear_eq_to_matrix([eqps2&C.i,eqps2&C.j,eqps2&C.k,divpsi2],varp)



        sol = np.zeros(len(expeq), dtype=object)
        eig = np.zeros(len(expeq), dtype=object)
        sol_psi = np.zeros(len(expeq), dtype=object)
        eig_psi = np.zeros(len(expeq), dtype=object)
        sol_psi2 = np.zeros(len(expeq), dtype=object)
        eig_psi2 = np.zeros(len(expeq), dtype=object)

        if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == "layer":
            sol_psi_ins = np.zeros(len(expeq), dtype=object)
            eig_psi_ins = np.zeros(len(expeq), dtype=object)
            psiBnd_insu = psi_insu

        UBnd = U
        BBnd = B
        rhoBnd = rho
        psiBnd = psi
        psiBnd_2 = psi_2b



        for j, ansatz in enumerate(expeq):
            varom = var

            rprint('[purple]ANSATZ : exp('+pretty(expand(log(ansatz),force=True).xreplace(dicprint))+')')

            
            def coe(x): return x.coeff(ansatz)
            eqp = eqh.applyfunc(coe)


            tilt = (simplify(((diff(ansatz,x)*C.i+diff(ansatz,y)*C.j )/ ansatz)
                             )).to_matrix(C).evalf()
            
            if tilt[0] != 0 and tilt[1] == 0 :
                eqp.row_del(1)
                eqp.row_del(4)
            elif tilt[0] == 0 and tilt[1] != 0 :
                eqp.row_del(2)
                eqp.row_del(5)
            elif tilt[0] == 0 and tilt[1] == 0:
                eqp.row_del(3)
                eqp.row_del(6)
                eqp.row_del(6)
                varom = varom[:-1]
            else:
                eqp.row_del(1)
                eqp.row_del(4)
            Mv,ruu = linear_eq_to_matrix(eqp,varom)
            Mv = Mv.xreplace({y: 0,z:0})

            sol[j], eig[j], M1 = eigen(Mv, ansatz, DIC)


#############################################################################
            """ Calculate kz for top and bottom boundary using same method that in the
                fluid using induction equation for solid """

            rprint("~~~~ KZ - TOP BOUNDARY ~~~~")
            ##### IN TOP BOUNDARY
            MAc= Mpsi.applyfunc(coe)

            sol_psi[j],eig_psi[j],M1_psi = eigen(MAc,ansatz,DIC,top =True)
            sol_psi[j] = sol_psi[j][np.imag(np.array(sol_psi[j],dtype=np.complex128)) >= 0 ]

            if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == 'layer':
                MAc= Mpsi.applyfunc(coe)
                sol_psi[j],eig_psi[j],M1_psi = eigen(MAc,ansatz,DIC,top =True)

                MAc= Mpsi2.applyfunc(coe)
                sol_psi_ins[j],eig_psi_ins[j],M1_psi_ins = eigen(MAc,ansatz,DIC,top =True)
                sol_psi_ins[j] = sol_psi_ins[j][np.imag(np.array(sol_psi_ins[j],dtype=np.complex128)) >= 0 ]
                psiBnd_insu = psiBnd_insu + ev**iv * et**it * (Symbol('bm_i' + str(i) + 'x')*C.i+
                Symbol('bm_i' + str(i) + 'y')*C.j+Symbol('bm_i' + str(i) + 'z')*C.k).xreplace(makedic_ins(veigen_ins(eig_psi_ins[j], sol_psi_ins[j], ansatz), i))


            if Bound_nb == 2:
                rprint("~~~~ KZ - BOTTOM BOUNDARY ~~~~")
                MAc2= Mpsi2.applyfunc(coe)
                sol_psi2[j],eig_psi2[j],M1_psi2 = eigen(MAc2,ansatz,DIC)
                sol_psi2[j] = sympify(sol_psi2[j][np.imag(np.array(sol_psi2[j],dtype=np.complex128)) <= 0 ])
                psiBnd_2 = psiBnd_2 + ev**iv * et**it * (Symbol('bm2' + str(i) + 'x')*C.i+Symbol('bm2' + str(i) + 'y')*C.j+Symbol('bm2' + str(i) + 'z')*C.k).xreplace(makedic_IC(veigen_IC(eig_psi2[j], sol_psi2[j], ansatz), i))

            # CREATE VARIABLES

            Uhom = ev**iv * et**it * (((Symbol('u' + str(i) + 'x') * C.i + Symbol(
                'u' + str(i) + 'y') * C.j + Symbol('u' + str(i) + 'z') * C.k))).xreplace(makedic(veigen(eig[j], sol[j], ansatz), i))
            Bhom = ev**iv * et**it * (((Symbol('b' + str(i) + 'x') * C.i + Symbol(
                'b' + str(i) + 'y') * C.j + Symbol('b' + str(i) + 'z') * C.k))).xreplace(makedic(veigen(eig[j], sol[j], ansatz), i))
            rhohom = ev**iv * et**it * (Symbol('rho' + str(i))).xreplace(makedic(veigen(eig[j], sol[j], ansatz), i))
            psihom =ev**iv * et**it * (Symbol('bm' + str(i) + 'x')*C.i+Symbol('bm' + str(i) + 'y')*C.j+Symbol('bm' + str(i) + 'z')*C.k).xreplace(makedic_mantle(veigen_mantle(eig_psi[j], sol_psi[j], ansatz), i))


            UBnd  += Uhom
            BBnd += Bhom
            rhoBnd += rhohom
            psiBnd += psihom

        UBnd += ev**iv * et**it *Usopart
        BBnd += ev**iv * et**it *Bsopart
        rhoBnd += ev**iv * et**it *rhosopart


        ########################################################################
        # Make Boundary condition equations
        condB = eval(config.get('BOUNDARY.CONDITIONS','B'))
        condU = eval(config.get('BOUNDARY.CONDITIONS','U'))
        rprint(Panel(Text(f"||| BOUNDARY CONDITIONS: {condU} and {condB} |||", justify="center"),border_style = 'blue'))
        Eqbound = Bound_nosolve(UBnd, BBnd,rhoBnd, psiBnd, psiBnd_2, {
            **DIC, **DIC}, iv, it)
        # Declare coefficient to find for BC problem
        if Bound_nb == 1:
            coeffs = [Symbol('C' + str(i)) for i in range(np.max([so.shape[0] for so in sol]))
                      ] + [Symbol('Cm' + str(i)) for i in range(np.max([so.shape[0] for so in sol_psi]))]
        if Bound_nb == 2:
            coeffs = [Symbol('C' + str(i)) for i in range(np.max([so.shape[0] for so in sol]))] + [Symbol('Cm' + str(i)) for i in range(np.max([so.shape[0] for so in sol_psi]))]+ [Symbol('Cc' + str(i)) for i in range(np.max([so.shape[0] for so in sol_psi2]))]

        # create matrix from BC
        Matbtot, resbtot = linear_eq_to_matrix(Eqbound, coeffs)
        Matbtot = Matbtot
        resbtot = resbtot
        Matcount = Matbtot
        rescount = resbtot


        for j, ansatz in enumerate(expeq):
            rprint('[purple]ANSATZ : exp('+pretty(expand(log(ansatz),force=True).xreplace(dicprint))+')')
            if ansatz.xreplace({z: 0}).as_independent(x,y,t,as_Add=True)[1] == 0:
                def coe(dd): return dd.as_independent(x,y,t,as_Add=True)[0]
            else:
                def coe(dd): return dd.coeff(ansatz.xreplace({z: 0}))
            Mat = (Matbtot).applyfunc(coe)
            res = (resbtot).applyfunc(coe)

            Mat = Mat.xreplace({y:0,z:0})
            res = res.xreplace({y:0,z:0})
            abc = solve_linear(Mat,res,test = True)
            abc = Matrix(abc)
            Mat = Matrix(Mat)
            res = Matrix(res)

            Matcount -= Mat * (ansatz.xreplace({z: 0}))
            rescount -= simplify(res * (ansatz.xreplace({z: 0})))
            dic_const = dict(zip(coeffs, abc))

            U += (ev**iv * et**it * (((Symbol('u' + str(i) + 'x') * C.i + Symbol(
                'u' + str(i) + 'y') * C.j + Symbol('u' + str(i) + 'z') * C.k))).xreplace(makedic(veigen(eig[j], sol[j], ansatz), i))).xreplace(dic_const)
            B += (ev**iv * et**it * (((Symbol('b' + str(i) + 'x') * C.i + Symbol(
                'b' + str(i) + 'y') * C.j + Symbol('b' + str(i) + 'z') * C.k))).xreplace(makedic(veigen(eig[j], sol[j], ansatz), i))).xreplace(dic_const)

            rho += (ev**iv * et**it * (Symbol('rho' + str(i))).xreplace(makedic(veigen(eig[j], sol[j], ansatz), i))).xreplace(dic_const)
            psi += (ev**iv * et**it * (Symbol('bm' + str(i) + 'x')*C.i+Symbol('bm' + str(i) + 'y')*C.j+Symbol('bm' + str(i) + 'z')*C.k).xreplace(makedic_mantle(veigen_mantle(eig_psi[j], sol_psi[j], ansatz), i))).xreplace(dic_const)

            if Bound_nb == 2:
                psi_2b += (ev**iv * et**it * (Symbol('bm2' + str(i) + 'x')*C.i+Symbol('bm2' + str(i) + 'y')*C.j+Symbol('bm2' + str(i) + 'z')*C.k).xreplace(makedic_IC(veigen_IC(eig_psi2[j], sol_psi2[j], ansatz), i))).xreplace(dic_const)


        rescount = simplify(rescount)#.xreplace({x: 1})
        if config.getboolean('OPTIONS','test') == 1:
            rprint('Matcount', simplify(simplify(Matcount)))
            rprint('rescount ::::::::::::::::::::', rescount)

        U = U.xreplace(DIC)+ ev**iv * et**it *Usopart
        B = B.xreplace(DIC)+ ev**iv * et**it *Bsopart
        p = sympify(p).xreplace(DIC)
        rho = rho.xreplace(DIC)+ ev**iv * et**it *rhosopart
        psi = psi.xreplace(DIC)
        psi_2b = sympify(psi_2b).xreplace(DIC)
    return(U, B, p, rho, psi, psi_2b)


def testBC(U, B, psi, psi_2b, dic, iv, it):
    """Test if boundary conditions are solved at given order

    Attrs:
    - U: Vector, velocity field.
    - B: Vector, magnetic field.
    - p: Expr, pressure field.
    - rho: Expr, density field.
    - psi: - Vector, magnetic field in the mantle, if conducting,
           - Expr, Scalar potential of the magnetic field, if insulating.
    - psi_2b: same as psi in the 2nd boundary.
    - dic: dict, dictionary containing all base fields information:
    - iv: int, order associated to small parameter ev.
    - it: int, order associated to small parameter et.

    Returns:
    ()
    """
    testbound = Bound_nosolve(U, B,rho, psi, psi_2b, DIC, iv, it)

    testbound = testbound.xreplace({x: mp.pi / 4, y: 0, t: 0}).evalf()

    testbound = mp.chop(mpmathM(testbound.evalf(3)),
                        tol=10**(-mp.mp.dps / 1.2))
    rprint('BC Order' + str(iv) + ',' + str(it))
    rprint(testbound)
    return(testbound)


def testEQ(U, B, rho, psi, psi_2b, dic, iv, it):
    """Test if equations are solved at given order

    Attrs:
    - U: Vector, velocity field.
    - B: Vector, magnetic field.
    - p: Expr, pressure field.
    - rho: Expr, density field.
    - psi: - Vector, magnetic field in the mantle, if conducting,
           - Expr, Scalar potential of the magnetic field, if insulating.
    - psi_2b: same as psi in the 2nd boundary.
    - dic: dict, dictionary containing all base fields information:
    - iv: int, order associated to small parameter ev.
    - it: int, order associated to small parameter et.

    Returns:
    ()
    """
    testequations = makeEquations(U, B, rho, iv, it, DIC)
    if eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == 'insulator':
        Bm = psi
        mantle = curl(Bm)
    elif eval(config.get('BOUNDARY.CONDITIONS','B'))[0] == 'conductor':
        Bm = psi
        mantle = diff(Bm, t) - qRmm * laplacian(Bm)
    divBm = divergence(Bm)
    if iv == 'no':
        mantle = [mantle&C.i,mantle&C.j,mantle&C.k,divBm]
    else:
        mantle = [taylor(mantle&C.i,iv,it,DIC),taylor(mantle&C.j,iv,it,DIC),taylor(mantle&C.k,iv,it,DIC),taylor(divBm,iv,it,DIC)]

    testequations = Matrix(list(testequations) + mantle)
    testequations = testequations.evalf(subs= {x:0,z:0,y:0,t:0})
    testequations = mp.chop(
        mpmathM(testequations.evalf(3)), tol=10**(-mp.mp.dps / 1.2))
    radsimpprint('Equations Order ' + str(iv) + ',' + str(it))
    listEQ = ['div_U      ', 'Eq_vort_x      ', 'Eq_vort_y      ', 'Eq_vort_z   ', 'div_B      ','Eq_b_i    ', 'Eq_b_j    ', 'Eq_b_k    ', 'eq_rho     ', 'Eq_bm_x   ', 'Eq_bm_y   ', 'Eq_bm_z   ', 'div_BM   ']

    for i,j in zip(listEQ,list(testequations)):
        rprint(i,':',j)

    return(testequations)


def testRES_BC(U, B, p, rho, psi, psi_2b, dic):
    """Return residual of boundary conditions

    Attrs:
    - U: Vector, velocity field.
    - B: Vector, magnetic field.
    - p: Expr, pressure field.
    - rho: Expr, density field.
    - psi: - Vector, magnetic field in the mantle, if conducting,
           - Expr, Scalar potential of the magnetic field, if insulating.
    - psi_2b: same as psi in the 2nd boundary.
    - dic: dict, dictionary containing all base fields information:

    Returns:
    ()
    """
    rprint("Test residuals of U.n")
    testbound = Bound_nosolve(U, B,rho, psi, psi_2b, DIC, 'no', 'no')
    UN = testbound[0]
    ZETA_B = 1e-4
    LIM = 0.05
    import scipy
    sol = scipy.optimize.root(lambda x: intepi(UN, x) - LIM, ZETA_B)
    rprint(sol)
    rprint('RESFINAL', sol.fun + LIM)
    rprint('realres : ', intepi(UN, sol.x))
    rprint('ZETA FINAL', sol.x)


def testRES_EQ(U, B, p, rho, psi, psi_2b, dic):
    """Return residual of equations

    Attrs:
    - U: Vector, velocity field.
    - B: Vector, magnetic field.
    - p: Expr, pressure field.
    - rho: Expr, density field.
    - psi: - Vector, magnetic field in the mantle, if conducting,
           - Expr, Scalar potential of the magnetic field, if insulating.
    - psi_2b: same as psi in the 2nd boundary.
    - dic: dict, dictionary containing all base fields information:

    Returns:
    ()
    """
    rprint("Test residuals of NS")
    testeq = makeEquations(U, B, p, rho, 'no', 'no', DIC)
    UN = surfcond(sqrt(testeq[0]**2 + testeq[1]**2 + testeq[2]**2), DIC)
    ZETA_B = 1e-3
    LIM = 1e-5
    RES = 1
    import scipy
    sol = scipy.optimize.root(lambda x: intepi(UN, x) - LIM, ZETA_B)
    rprint('RESFINAL', sol.fun + LIM)
    rprint('ZETA FINAL', sol.x)


#########################
###     Variables     ###
#########################

order = config.getint('OPTIONS','order')
Bound_nb = config.getint('OPTIONS','Bound_nb')
buf = config.getboolean('OPTIONS','buf')
atmo = config.getboolean('OPTIONS','atmo')
x = C.x
y = C.y
z = C.z
x.is_real
y.is_real
z.is_real
x._assumptions['real'] = True
y._assumptions['real'] = True
z._assumptions['real'] = True
t = Symbol('t', real=True)
dicprint = {x:Symbol('x'),y:Symbol('y'),z:Symbol('z')}

Dist, et, ev = symbols("Dist,epsilon_t,epsilon_v", real=True)
Ri, Ro, Al, Rm, omega,  kx, ky, kxl, kyl, kz, rho_r, alpha, g, zeta, g0, g1, buoy, a, b, c, dv, fv, BOx, BOy, BOz, psi0, psi0_2b, p0,rho0 = symbols(
    "Ri, Ro, Al, Rm, omega, kx,ky,kxl,kyl,kz,rho_r,alpha, g,zeta, g0, g1, buoy, a, b, c, dv, fv,BOx, BOy, BOz,psi0,psi0_2b,p0,rho0")
qRm, qRe, qRo, qFr, chi, qAl, qRmm, qRmc, Rl,fC,qRer = symbols(
    "qRm,qRe,qRo,qFr,chi,qAl,qRmm,qRmc,Rl,fC,qRer")
bmx, bmy, bmz = symbols("bmx,bmy,bmz")
f0 = Function("f0")(x, y, z, t)
f1 = Function("f1")(x, y, z, t)
f2 = Function("f2")(x, y, z, t)
f0_2 = Function("f0_2")(x, y, z, t)
f1_2 = Function("f1_2")(x, y, z, t)

def AgradB(A,B):
    aB = (((A&C.i) * diff(B&C.i, x) + (A&C.j) * diff(B&C.i, y) + (A&C.k) * diff(B&C.i, z)) * C.i
            + ((A&C.i) * diff(B&C.j, x) + (A&C.j) * diff(B&C.j, y) + (A&C.k) * diff(B&C.j, z)) * C.j
            + ((A&C.i) * diff(B&C.k, x) + (A&C.j) * diff(B&C.k, y) + (A&C.k) * diff(B&C.k, z)) * C.k)
    return(aB)

b0x = Function("b0x")(x, y, z, t)
b0y = Function("b0y")(x, y, z, t)
b0z = Function("b0z")(x, y, z, t)
u0x = Function("u0x")(x, y, z, t)
u0y = Function("u0y")(x, y, z, t)
u0z = Function("u0z")(x, y, z, t)
f0 = Function("f0")(x, y, z, t)
f1 = Function("f1")(x, y, z, t)
f2 = Function("f2")(x, y, z, t)
f0_2 = Function("f0_2")(x, y, z, t)
f1_2 = Function("f1_2")(x, y, z, t)
f2_2 = Function("f2_2")(x, y, z, t)

u0 = Function("u0x")(x, y, z, t) * C.i + Function("u0y")(x, y,
                                                         z, t) * C.j + Function("u0z")(x, y, z, t) * C.k
b0 = Function("b0x")(x, y, z, t) * C.i + Function("b0y")(x, y,
                                                         z, t) * C.j + Function("b0z")(x, y, z, t) * C.k

####################################
##### Definition and test ##########
####################################

locals = {'x':C.x,'y':C.y,'z':C.z,'et':et}
dic = dict(config['PARAMETERS'])
THETA = sympify(config.get('PARAMETERS','THETA'),rational=True,locals=locals)
PHI = sympify(config.get('PARAMETERS','PHI'),rational=True,locals=locals)
DIC = {
    u0x: sympify(config.get('PARAMETERS','U0'),rational=True,locals=locals)[0],
    u0y:sympify(config.get('PARAMETERS','U0'),rational=True,locals=locals)[1],
    u0z: sympify(config.get('PARAMETERS','U0'),rational=True,locals=locals)[2],
    b0x: sympify(config.get('PARAMETERS','U0'),rational=True,locals=locals)[0],
    b0y:sympify(config.get('PARAMETERS','U0'),rational=True,locals=locals)[1],
    b0z: sympify(config.get('PARAMETERS','U0'),rational=True,locals=locals)[2],
    fC : matrix_to_vector(sympify(config.get('PARAMETERS','Omega'),rational=True,locals=locals),C),
    f0: z,
    f1:- sympify(config.get('PARAMETERS','h'),rational=True,locals=locals),
    f0_2: z,
    f1_2:- sympify(config.get('PARAMETERS','h2'),rational=True,locals=locals),
    qRe: 1/sympify(config.get('PARAMETERS','Re'),rational=True,locals=locals), 
    qRo:1/sympify(config.get('PARAMETERS','Ro'),rational=True,locals=locals),
    Rl: sympify(config.get('PARAMETERS','chi'),rational=True,locals=locals),
    qRer:1/sympify(config.get('PARAMETERS','Rer'),rational=True,locals=locals),
    qRmc: 1/sympify(config.get('PARAMETERS','Rmc'),rational=True,locals=locals),
    qRm: 1/sympify(config.get('PARAMETERS','Rm'),rational=True,locals=locals),
    qRmm: 1/sympify(config.get('PARAMETERS','Rmm'),rational=True,locals=locals),
    qAl: 1/sympify(config.get('PARAMETERS','Al'),rational=True,locals=locals),
    qFr: 1/sympify(config.get('PARAMETERS','Fr'),rational=True,locals=locals),
    Dist: sympify(config.get('PARAMETERS','ht'),rational=True,locals=locals),
}

rprint(Panel(Text(f"||| BASIC FIELDS |||", justify="center"),border_style = 'blue'))
rprint(f'θ = {THETA}, φ = {PHI}')
ansatz = 0
rho0 = sympify(0)
g = -g0 * C.k
f = f0 + f1
f= f.xreplace(DIC).rewrite(exp)

# Vector normal and tangential to the topography
delf = gradient(f)
nfo = delf.normalize()
nx = (nfo & C.i)
ny = (nfo & C.j)
nz = (nfo & C.k)

if DIC[f1] != 0:
    tfox = (C.i) + ((ny / nx) * C.j) + (-((nx**2 + ny**2) / (nx * nz)) * C.k)
    tfox = tfox.normalize()
    tfoy = (-(ny / nx) * C.i) + (C.j) + (0 * C.k)
    tfoy = tfoy.normalize()
else:
    tfox = (C.i)
    tfoy = (C.j)

if Bound_nb == 2:
    f_2 = f0_2 + Dist + f1_2
    f_2 = f_2.xreplace(DIC).rewrite(exp)
    delf2 = gradient(f_2)
    nfo2 = -delf2.normalize()
    nx2 = (nfo2 & C.i)
    ny2 = (nfo2 & C.j)
    nz2 = (nfo2 & C.k)
    if DIC[f1_2] != 0:
        tfox2 = (C.i) + ((ny2 / nx2) * C.j) + \
            (-((nx2**2 + ny2**2) / (nx2 * nz2)) * C.k)
        tfox2 = -tfox2.normalize()
        tfoy2 = (-(ny2 / nx2) * C.i) + (C.j) + (0 * C.k)
        tfoy2 = tfoy2.normalize()
    else:
        tfox2 = (C.i)
        tfoy2 = (C.j)


######################
###     SCRIPT     ###
######################

# CHECK the realness of imposed Field
if config.getboolean('OPTIONS','test') == 1:
    testre = Matrix([DIC[u0x], DIC[u0y], DIC[u0z], DIC[b0x],
                    DIC[b0y], DIC[b0z], DIC[f1], DIC[f1_2]]).xreplace(DIC)
    testre = np.imag(np.sum(np.array((testre.xreplace(
        {x: 10, y: 10, t: 10, omega: 10, ev: 1,z:-1,Rl:1e-4,et:1}).evalf()), dtype=np.complex128)))
    if testre > 1e-15:
        rprint('One of the parameters is not real')
        raise ValueError

if DIC[qRe] != 0 and eval(config.get('BOUNDARY.CONDITIONS','U')) == 'inviscid':
    rprint("Warning inviscid BC incompatible with qRe != 0")
    rprint("qRe set to 0")
    DIC[qRe] = 0

#################

n = nfo.xreplace(DIC
                 ).doit().xreplace(DIC)

# 1st tangential vector
tx = tfox.xreplace(DIC
                   ).doit().xreplace(DIC)
# 2nd tangential vector
ty = tfoy.xreplace(DIC
                   ).doit().xreplace(DIC)

# same for the 2nd boundary
if Bound_nb == 2:
    n2 = nfo2.xreplace(DIC
                       ).doit().xreplace(DIC)

    tx2 = (tfox2.xreplace(DIC)
           ).doit().xreplace(DIC)
    ty2 = tfoy2.xreplace(
        DIC).doit().xreplace(DIC)

U, B, p, rho, psi, psi_2b = BaseField(DIC)

if type(order) == int:
    test_e = expand(sympify(U).xreplace(DIC)).coeff(ev)#+expand(sympify(rho0*C.i).xreplace(DIC)).coeff(ev)

    if test_e == 0:
        ev_test = False
    else:
        ev_test = True

    if ev_test == True:
        lim_part = 4
    else:
        lim_part = 2

    lim_part = 0
    orderB = order
    parallel = False

    # ev_test = True
    if ev_test == True:
        comb = [(0,0),(1,0)]
        for i in range(orderB + 1):
            i +=1
            comb += list(itertools.combinations_with_replacement(np.arange(1, i+1), 2))
        ZU, indice = np.unique(comb, axis=0, return_index=True)
        comb = np.array(comb)[np.sort(indice)]
        comb = comb[np.sum(comb, axis=1) <= orderB]
        order_list = [
            np.unique(list(itertools.permutations(co, 2)), axis=0) for co in comb]
        if parallel == False:
            orde = []
            for o1 in order_list:
                for o2 in o1:
                    orde += [[o2]]
            order_list = orde

    elif ev_test == False:
        list_1 = np.zeros(len(np.arange(order + 1)), dtype=np.int64)
        list_2 = np.arange(order + 1)
        comb = np.unique(np.array(list(itertools.product(list_1, list_2))), axis=0)
        aa = (np.sum(comb, axis=1)) * 10 + np.abs(comb[:, 1] - comb[:, 0])
        idx = aa.argsort()
        order_list = np.array([np.array([co]) for co in comb[idx]])
elif type(order) == np.ndarray:
    order_list = order

else:
    raise NameError('wrong input for orders')

rprint(f'Orders : {[list(ord[0]) for ord in order_list]}')

U, B, p, rho, psi, psi_2b = BaseField(DIC)
U0_c = U
B0_c = B
rho0_c = rho
start_time = time.time()
for i in range(0, len(order_list)):

    iv = order_list[i][0][0]
    it = order_list[i][0][1]

    U, B, p, rho, psi, psi_2b = compute_order(
        U, B, p, rho, psi, psi_2b, DIC, iv, it)

    expo_fluid = findexp(Matrix([simp(U & C.i)]))
    expo_fluid = [sympify(expand_log(log(ex),force=True)).evalf() for ex in expo_fluid]
    expo_fluid = np.array(expo_fluid,dtype = object)

    expo_top = findexp(Matrix([simp(psi & C.i)]))
    expo_top = [sympify(expand_log(log(ex),force=True)).evalf() for ex in expo_top]
    expo_top = np.array(expo_top,dtype = object)


    if config.getboolean('OPTIONS','test') == True:
        testBC(U, B, psi, psi_2b, DIC, iv, it)
        testEQ(U, B,rho, psi, psi_2b, DIC, iv, it)
        
    if config.getboolean('OPTIONS','calculate_stress') == True:
        P=0
        B = simp(B)
        expo = findexp(B.to_matrix(C))
        Cor = (2 * qRo * fC.xreplace(DIC)).cross(U)
        UgradU  = AgradB(U,U)
        JB = curl(B).cross(B)
        if buf == 1:
            UgradU = 1e-10 * C.i
        if atmo == 1 or buf ==1:
            JB = 1e-10 * JB
        if eval(config.get('BOUNDARY.CONDITIONS','U')) == 'inviscid':
            Eq_NS = diff(U, t) + Cor +UgradU - (  (rho*C.k) + JB)
        else:
            Eq_NS = diff(U, t) + Cor  +UgradU - (
                                                    qRe * laplacian(U) + (rho*C.k) + JB)

        Eq_NS = simp(taylor(Eq_NS&C.i, iv, it, DIC))*C.i+simp(taylor(Eq_NS&C.j, iv, it, DIC))*C.j+simp(taylor(Eq_NS&C.k, iv, it, DIC))*C.k
        expo = list(findexp(simp(Eq_NS.to_matrix(C))))+[0]


        for ex in expo:
            P += Symbol('p')*ex
        Eq_NS += gradient(P)
        for ex in expo:
            E1 = (simp(Eq_NS&C.i)).coeff(ex)
            E2 = (simp(Eq_NS&C.j)).coeff(ex)
            E3 = (simp(Eq_NS&C.k)).coeff(ex)

            try:
                tilt = (simplify((gradient(ex) / ex)
                             ).normalize()).to_matrix(C)
            except:
                tilt = [1,0,0]

            if tilt[0] != 0 and tilt[1] == 0 :
                Mp,rp = linear_eq_to_matrix([E1],[Symbol('p')])
            elif tilt[0] == 0 and tilt[1] != 0 :
                Mp,rp = linear_eq_to_matrix([E2],[Symbol('p')])
            elif tilt[0] == 0 and tilt[1] == 0:
                Mp,rp = linear_eq_to_matrix([E3],[Symbol('p')])
            else:
                Mp,rp = linear_eq_to_matrix([E1],[Symbol('p')])

            Mp = Mp.xreplace({y:0,z:0})
            rp = rp.xreplace({y:0,z:0})
            soo = solve_linear(Mp,rp,test = True)[0]
            p += ev**iv*et**it*soo*ex

        if config.getboolean('OPTIONS','test') ==True:
            rprint('TEST')
            UgradU  = AgradB(U,U)
            Cor = ((2 * qRo * fC.xreplace(DIC)).cross(U)).xreplace(DIC)
            JB = curl(B).cross(B)
            if buf == 1:
                UgradU = 1e-10 * C.i
            if atmo == 1 or buf ==1:
                JB = 1e-10 * JB
            if eval(config.get('BOUNDARY.CONDITIONS','U')) == 'inviscid':
                Eq_NS = diff(U, t) + Cor +UgradU +gradient(p) - (  (rho*C.k) + JB)
            else:
                Eq_NS = diff(U, t) + Cor  +UgradU +gradient(p) - (
                                                        qRe * laplacian(U) + (rho*C.k) + JB)

            Eq_NS = simp(taylor(Eq_NS&C.i, iv, it, DIC))*C.i+simp(taylor(Eq_NS&C.j, iv, it, DIC))*C.j+simp(taylor(Eq_NS&C.k, iv, it, DIC))*C.k
            rprint(Eq_NS.xreplace({x:0,y:0,z:0,t:1}).evalf(2))

    U = U.xreplace(DIC)
    B = B.xreplace(DIC)
    rho = sympify(rho).xreplace(DIC)
    p = sympify(p).xreplace(DIC)

    ########################
    ###   Post Process   ###
    ########################
    solfull = Matrix([U & C.i, U & C.j, U & C.k, p,
                    B & C.i, B & C.j, B & C.k, rho, psi, psi_2b]).xreplace(DIC)
    ###############################
    expo_final = findexp(Matrix([simp(U & C.i)]))
    epp = [sympify(expand_log(log(ex),force=True)).evalf(4) for ex in expo_final]
    expo_final = [sympify(sympify(expand_log(log(ex),force=True).as_independent(z)[1]).xreplace({z:1})).evalf() for ex in expo_final]
    expo_final = np.unique(np.array(expo_final,dtype = np.complex64))

    
    ###############################

    pw_kz=0
    order_list = np.array(order_list)
    if config.getboolean('OPTIONS','calculate_stress') == True:
        if config.getboolean('OPTIONS','decomp_kz') == True:
            pw_kz,Stress = Pressure_force.calc_stress(
                solfull, C, f.xreplace(DIC), order_list[i],eval(config.get('BOUNDARY.CONDITIONS','B')),DIC,config.getboolean('OPTIONS','decomp_kz'))
        else:
            Stress = Pressure_force.calc_stress(
                solfull, C, f.xreplace(DIC), order_list[i],eval(config.get('BOUNDARY.CONDITIONS','B')),DIC,config.getboolean('OPTIONS','decomp_kz'))

    if config.getboolean('OPTIONS','ohmic_dissipation') == True:

        ohmic_diss = Pressure_force.comp_Diss(solfull, C, f.xreplace(
            DIC), DIC[qRm], DIC[qRmm], order_list[i], eval(config.get('BOUNDARY.CONDITIONS','B')), DIC)
        
    # print('__________ EXP __________')
    # for ep in epp:
    #     rprint(pretty(ep.xreplace(dicprint)))
    # print('_________________________')
    ###############################################
    ##############    WRITE FILE    ###############
    ###############################################
    wri = config.get('OPTIONS','write_file')
    try:    
        wri = eval(wri)
    except:
        pass

    if wri == True or wri == 'fast':
        rprint('#### SAVE FILE ####')
        topo1 = sympify(config.get('PARAMETERS','h'),rational=True,locals=locals)
        dic_meta = {'U0x': sympify((DIC[u0x]).xreplace(DIC),rational=True),
                    'U0y': sympify((DIC[u0y]).xreplace(DIC),rational=True),
                    'B0x': sympify((qAl * DIC[b0x]).xreplace(DIC),rational=True),
                    'B0y': sympify((qAl * DIC[b0y]).xreplace(DIC),rational=True),
                    'B0z': sympify((qAl * DIC[b0z]).xreplace(DIC),rational=True),
                    'Ro': 1 / sympify(DIC[qRo],rational=True),
                    'THETA': THETA,
                    'PHI':PHI,
                    'Al': 1 / sympify(DIC[qAl],rational=True),
                    'Fr': 1 / sympify(DIC[qFr],rational=True),
                    'Rm': 1 / sympify(DIC[qRm],rational=True),
                    'Rmm': 1 / sympify(DIC[qRmm],rational=True),
                    'Rmc': 1 / sympify(DIC[qRmc],rational=True),
                    'Re': 1 / sympify(DIC[qRe],rational=True),
                    'Rer': 1 / sympify(DIC[qRer],rational=True),
                    'chi':  sympify(DIC[Rl],rational=True),
                    'fC' : sympify(DIC[fC].xreplace(DIC)),
                    'condB': eval(config.get('BOUNDARY.CONDITIONS','B')),
                    'atmo': config.getboolean('OPTIONS','atmo'),
                    'order_list': order_list,
                    'zscales':expo_final,
                    'nbzscales':int(len(expo_final)),
                    'pw_kz':pw_kz,
                    'Dist':Dist.xreplace(DIC),
                    'time': (time.time() - start_time)}
        data = {
            'topo': topo1,
            'meta': dic_meta}
        rprint(data)
        if config.getboolean('OPTIONS','calculate_stress') == True:
            data['Stress'] = Stress


        if config.getboolean('OPTIONS','ohmic_dissipation') == True:
            data['Ohm_Diss'] = ohmic_diss

        if Bound_nb == 2:
            topo2 = (-(f_2 - f0_2)).xreplace(DIC)
            data['topo2'] = topo2


        path = os.path.dirname(config.get('OPTIONS','filename'))
        if path != '':
            isExist = os.path.exists(path)
            if isExist == False:
                os.makedirs(path)
        finam = config.get('OPTIONS','filename') + '_order'+str(iv)+'_'+str(it)+'.dat'
        rprint(finam)
        with open(finam, "wb") as filedumped:
            pickle.dump(data, filedumped,protocol=5)
            filedumped.close()

        rprint('Output saved')
rprint('Calculation finished successfully')
