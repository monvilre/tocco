#! /usr/bin/env python3
# coding: utf-8
""" Tools for visualization with ToCCo output """
__author__ = "Rémy Monville"

import numpy as np
from sympy import *
import mpmath as mp
from mpmath import mpf,mpc
from sympy.vector import CoordSys3D,matrix_to_vector,curl,gradient,divergence,Del,Divergence,Gradient, laplacian,Curl
import matplotlib.pyplot as plt
from matplotlib import animation, rc
# from cmcrameri import cm as SCM
import pickle
import matplotlib.animation as animation
import random
import matplotlib as mpl

from tocco.mod import *


def import_data(filename):
    # """ import for sympy """
    # U0,omega,qRe,chi,qRo,BOx,qAl,BOy,BOz,qRm,qRmm,qFr,Ri,zeta,Dist,e,t,p0,xx,yy,CUx,CUy,CUz,CBx,CBy,CBz,et,ev,tau0,qRmc,Rl = symbols("U0,omega,qRe,chi,qRo,BOx,qAl,BOy,BOz,qRm,qRmm,qFr,Ri,zeta,Dist,e,t,p0,xx,yy,CUx,CUy,CUz,CBx,CBy,CBz,et,ev,tau0,qRmc,Rl")
    # C = CoordSys3D('C')
    # x = C.x
    # y = C.y
    # z = C.z
    # x.is_real
    # y.is_real
    # z.is_real
    # x._assumptions['real'] = True
    # y._assumptions['real'] = True
    # z._assumptions['real'] = True
    # t = Symbol('t', real=True)
    # et,ev = symbols('et,ev',real=True)
    with open(filename, 'rb') as f:
        data = pickle.load(f)
    dic = data['meta']
    solfull = data['Expr']

    return(solfull,dic)


def lambda_func(input,prec=False):
    if prec== True:
        fu = lambdify([x,y,z,t,ev,et],input,'mpmath')
        fu = np.vectorize(fu)
    else:
        fu = lambdify([x,y,z,t,ev,et],input,[{'sqrt': np.emath.sqrt},'scipy','numpy'])
    return(fu)



def Field(X,Y,Z,T,Ev,Et,lfunc_M,lfunc_OC,topo,topo2=None,lfunc_IC=None):
    topo = lambdify([x,y,t,et],re(topo))
    Var_tot = np.zeros(np.shape(X))
    M = (Z-topo(X,Y,T,Et) >=0)
    if topo2 != None:
        topo2 = np.vectorize(lambdify([x,y,t,et],re(topo2)))
        OC = ((Z-topo(X,Y,T,Et) <=0) * (Z-topo2(X,Y,T,Et) >=0))
        IC = (Z-topo2(X,Y,T,Et) <=0)
    else:
        OC = (Z-topo(X,Y,T,Et) <=0)

    if lfunc_M != None:
        Var_tot[M] = np.array(lfunc_M(X[M],Y[M],Z[M],T,Ev,Et),dtype=np.complex128)
    else:
        Var_tot[M] =np.nan

    if lfunc_OC != None:

        luf = np.array(lfunc_OC(X[OC],Y[OC],Z[OC],T,Ev,Et),dtype=np.complex128)
        Var_tot[OC] = luf
    else:
        Var_tot[OC] =np.nan

    if topo2 != None:
        if lfunc_IC != None:
            Var_tot[IC] = np.array(lfunc_IC(X[IC],Y[IC],Z[IC],T,Ev,Et),dtype=np.complex128)
        else:
            Var_tot[IC] =np.nan
    return(Var_tot)


def cross_section(file,X,Y,Z,scalar_field = 'Uz',streamlines = None,direction='x'):
    solfull,dic,topo,blc = import_data(file)
    U = solfull[0]
    B = solfull[2]
    p = solfull[1]
    if scalar_field == 'Ux':
        sF = U&C.i
    elif scalar_field == 'Uy':
        sF = U&C.j
    elif scalar_field == 'Uz':
        sF = U&C.k
    elif scalar_field == 'U':
        sF = sqrt(U.dot(U))
    elif scalar_field == 'Bx':
        sF = B&C.i
    elif scalar_field == 'By':
        sF = B&C.j
    elif scalar_field == 'Bz':
        sF = B&C.k
    elif scalar_field == 'B':
        sF = sqrt(B.dot(B))
    elif scalar_field[0] == 'J':
        J = curl(B)
        sF = J.dot(J)
    
    if streamlines == 'U':
        streamv = U
    if streamlines == 'B':
        streamv = B
    if streamlines == 'J':
        streamv = curl(B)

    if direction == 'x':
        sf1 = streamv&C.i
        sf2 = streamv&C.k
        C1,C2 = X,Z

    elif direction == 'y':
        sf1 = streamv&C.j
        sf2 = streamv&C.k
        C1,C2 = Y,Z
    else:
        sf1 = streamv&C.i
        sf2 = streamv&C.j
        C1,C2 = X,Y

    lfunc_v1 = viz.lambda_func(sf1)
    lfunc_v2 = viz.lambda_func(sf2)
    lfunc_S = viz.lambda_func(sF)

    
    
    



def cross_up(topo,dic,X,Y,Z,time,zev,zeta,lfunc_mantle,lfunc_core,funcstream_mantle,funcstream_core,di = 'x',cmap = 'viridis',cbarlabel = '/',**kwargs):


    V = Field(topo,lfunc_mantle,lfunc_core,X,Y,Z,time,zev,zeta)
    streamU = Field(topo,funcstream_mantle[0],funcstream_core[0],X,Y,Z,time,zev,zeta)#*Vscale
    streamV = Field(topo,funcstream_mantle[1],funcstream_core[1],X,Y,Z,time,zev,zeta)#*Vscale

    topography = topo(X,Y,time,zeta)*Xscale
    X = X*Xscale
    Y = Y*Xscale
    Z = Z*Xscale

    if 'vmin' in kwargs.keys():
        vmin = kwargs['vmin']
    else:
        vmin = np.nanmin(V)

    if 'vmax' in kwargs.keys():
        vmax = kwargs['vmax']
    else:
        vmax = np.nanmax(V)

    #plt.contour(X,Y,topography,20,colors = 'k')#,zorder=0)
    plt.contourf(X,Y,V,30,cmap = cmap,vmin = vmin,vmax=vmax)
    cb = plt.colorbar()
    # print(var)
    # print(Vscalestr)
    cb.set_label(cbarlabel, labelpad=10)

    stream_X = X
    stream_Y = Y

    plt.streamplot(stream_X,stream_Y,streamU,streamV,color = "k",linewidth = 0.8,density = (1.5,1.5),arrowsize = 0.8,minlength = 1)
    plt.streamplot(stream_X,stream_Y,streamU,streamV,color = "k",linewidth = 0.8,density = (1.5,1.5),arrowsize = 0.001,maxlength = 0.8)



    plt.xlabel('$x$ ')
    plt.ylabel('$y$ ')

def cross_section_dim(topo,dic,X,Y,Z,time,zev,zeta,lfunc_mantle,lfunc_core,funcstream_mantle,funcstream_core,di = 'x',cmap = 'viridis',cbarlabel = '/',**kwargs):
    "plot a cross section of choosen scalar field (var) + streamlines (varstream) "
    topo = lambdify([x,y,t,et],re(topo))
    # Xscale = np.float64(mp.re(dic['Rl']*2890000))
    # rhoscale=1e4
    # Xscale=1e5
    # Vscale = np.float64(7.29e-5*Xscale*dic['Ro'])
    # # Bscale = np.float64(Vscale*np.sqrt(rhoscale*4*np.pi*1e-7)/dic['Al'])
    # # Jscale = np.float64(Bscale/(Xscale*4*np.pi*1e-7))
    # Pscale = np.float64(Vscale**2*rhoscale)
    Xscale=1
    V = Field(topo,lfunc_mantle,lfunc_core,X,Y,Z,time,zev,zeta)
    streamU = Field(topo,funcstream_mantle[0],funcstream_core[0],X,Y,Z,time,zev,zeta)#*Vscale
    streamV = Field(topo,funcstream_mantle[1],funcstream_core[1],X,Y,Z,time,zev,zeta)#*Vscale

    topography = topo(X[0],Y[0],time,zeta)*Xscale
    X = X*Xscale
    Y = Y*Xscale
    Z = Z*Xscale
    if di == 'x':
        MAIN = X
    if di == 'y':
        MAIN = Y
    MAIN=MAIN#*1e-3

    if 'vmin' in kwargs.keys():
        vmin = kwargs['vmin']
    else:
        vmin = np.nanmin(V)

    if 'vmax' in kwargs.keys():
        vmax = kwargs['vmax']
    else:
        vmax = np.nanmax(V)


    plt.contourf(MAIN,Z,V,30,cmap = cmap,vmin = vmin,vmax=vmax)
    cb = plt.colorbar()
    # print(var)
    # print(Vscalestr)
    from matplotlib import ticker
    tick_locator = ticker.MaxNLocator(nbins=6)
    cb.locator = tick_locator
    cb.set_label(cbarlabel, labelpad=10)
    cb.update_ticks()



    stream_X = MAIN[0]
    stream_Y = Z[:,0]



    plt.streamplot(stream_X,stream_Y,streamU,streamV,color = "k",linewidth = 0.8,density = (1.1,1.1),arrowsize = 0.8,minlength = 1)
    plt.streamplot(stream_X,stream_Y,streamU,streamV,color = "k",linewidth = 0.8,density = (1.1,1.1),arrowsize = 0.001,maxlength = 0.8)
    # plt.streamplot(stream_X,stream_Y,streamU,streamV,color = "k",linewidth = 0.8,density = (1.2,2),arrowsize = 0.8,minlength = 0.02)


    plt.plot(MAIN[0],topography,'k',linewidth = 3)

    plt.xlabel('$x$ ')
    plt.ylabel('$z$ ')
    plt.ylim(np.min(Z),np.max(Z))
    plt.xlim(np.min(MAIN),np.max(MAIN))

def cross_section_dim_vector(topo,dic,X,Y,Z,time,zev,zeta,lfunc_mantle,lfunc_core,funcstream_mantle,funcstream_core,di = 'x',cmap = 'viridis',cbarlabel = '/',**kwargs):
    "plot a cross section of choosen scalar field (var) + streamlines (varstream) "
    topo = lambdify([x,y,t,et],re(topo))
    # Xscale = np.float64(mp.re(dic['Rl']*2890000))
    rhoscale=1e4
    Xscale=1e5
    Vscale = np.float64(7.29e-5*Xscale*dic['Ro'])
    # Bscale = np.float64(Vscale*np.sqrt(rhoscale*4*np.pi*1e-7)/dic['Al'])
    # Jscale = np.float64(Bscale/(Xscale*4*np.pi*1e-7))
    Pscale = np.float64(Vscale**2*rhoscale)

    V = Field(topo,lfunc_mantle,lfunc_core,X,Y,Z,time,zev,zeta)
    streamU = Field(topo,funcstream_mantle[0],funcstream_core[0],X,Y,Z,time,zev,zeta)#*Vscale
    streamV = Field(topo,funcstream_mantle[1],funcstream_core[1],X,Y,Z,time,zev,zeta)#*Vscale

    V = V*Pscale

    topography = topo(X[0],Y[0],time,zeta)*Xscale
    X = X*Xscale
    Y = Y*Xscale
    Z = Z*Xscale
    if di == 'x':
        MAIN = X
    if di == 'y':
        MAIN = Y
    MAIN=MAIN#*1e-3

    if 'vmin' in kwargs.keys():
        vmin = kwargs['vmin']
    else:
        vmin = np.nanmin(V)

    if 'vmax' in kwargs.keys():
        vmax = kwargs['vmax']
    else:
        vmax = np.nanmax(V)


    plt.contourf(MAIN,Z,V,30,cmap = cmap,vmin = vmin,vmax=vmax)
    cb = plt.colorbar()
    # print(var)
    # print(Vscalestr)
    from matplotlib import ticker
    tick_locator = ticker.MaxNLocator(nbins=6)
    cb.locator = tick_locator
    cb.set_label(cbarlabel, labelpad=10)
    cb.update_ticks()



    stream_X = MAIN[0]
    stream_Y = Z[:,0]

    # plt.streamplot(stream_X,stream_Y,streamU,streamV,color = "k",linewidth = 0.8,density = (1.1,1.1),arrowsize = 0.8,minlength = 1)
    # plt.streamplot(stream_X,stream_Y,streamU,streamV,color = "k",linewidth = 0.8,density = (1.1,1.1),arrowsize = 0.001,maxlength = 0.8)
    # plt.streamplot(stream_X,stream_Y,streamU,streamV,color = "k",linewidth = 0.8,density = (1.2,2),arrowsize = 0.8,minlength = 0.02)
    plt.quiver(stream_X[::10],stream_Y[::10],streamU[::10,::10],streamV[::10,::10], color = "k",angles= "xy")

    plt.plot(MAIN[0],topography,'k',linewidth = 3)

    plt.xlabel('$x$ ')
    plt.ylabel('$z$ ')
    plt.ylim(np.min(Z),np.max(Z))
    plt.xlim(np.min(MAIN),np.max(MAIN))

def stream3D(topo,dic,X,Y,Z,time,zev,zeta,funcstream_mantle,funcstream_core,di = 'x',cmap = 'viridis',cbarlabel = '/',**kwargs):
    import plotly.graph_objects as go
    topo = lambdify([x,y,t,et],re(topo))

    U = Field(topo,funcstream_mantle[0],funcstream_core[0],X,Y,Z,time,zev,zeta)#*Vscale
    V = Field(topo,funcstream_mantle[1],funcstream_core[1],X,Y,Z,time,zev,zeta)
    W = Field(topo,funcstream_mantle[2],funcstream_core[2],X,Y,Z,time,zev,zeta)

    Xscale = np.float64(mp.re(dic['Rl']*2890000))
    Xscale=1
    # Vscale = np.float64(7.29e-5*Xscale*dic['Ro'])
    # Bscale = np.float64(Vscale*np.sqrt(rhoscale*4*np.pi*1e-7)/dic['Al'])
    # Jscale = np.float64(Bscale/(Xscale*4*np.pi*1e-7))
    # Pscale = np.float64(Vscale**2*rhoscale)


    fig =go.Figure(data=go.Streamtube(x=X, y=Y, z=Z,
                                       u=U, v=V, w=W))
    fig.show()


def animation_cross(topo,dic,X,Y,Z,zev,zeta,lfunc_mantle,lfunc_core,funcstream_mantle,funcstream_core,filename,frames=24,di = 'x',cmap = 'viridis',cbarlabel = '/',**kwargs):
    omeg =  np.float64(mp.re(dic['omega']))
    time = np.linspace(0,np.pi*2/omeg,frames)
    topo = lambdify([x,y,t,et],re(topo))
    Xscale = np.float64(mp.re(dic['Rl']*2890000))


    V = np.array([Field(topo,lfunc_mantle,lfunc_core,X,Y,Z,ti,zev,zeta) for ti in time])
    topography = np.array([topo(X[0],Y[0],ti,zeta)for ti in time])*Xscale
    streamU = np.array([Field(topo,funcstream_mantle[0],funcstream_core[0],X,Y,Z,ti,zev,zeta) for ti in time])#*Vscale
    streamV = np.array([Field(topo,funcstream_mantle[1],funcstream_core[1],X,Y,Z,ti,zev,zeta) for ti in time])#*Vscale
    X = X*Xscale
    Y = Y*Xscale
    Z = Z*Xscale
    if di == 'x':
        MAIN = X
    if di == 'y':
        MAIN = Y
    MAIN=MAIN*1e-3

    stream_X = MAIN[0]
    stream_Y = Z[:,0]

#######################################
    if 'vmin' in kwargs.keys():
        vmin = kwargs['vmin']
    else:
        vmin = np.nanmin(V)

    if 'vmax' in kwargs.keys():
        vmax = kwargs['vmax']
    else:
        vmax = np.nanmax(V)
##########################################################################
# Création de la figure et de l'axe

    fig, ax = plt.subplots(figsize=(15,8))

    levels = np.linspace(vmin, vmax,30)
    kw = dict(levels=levels, cmap=cmap, vmin=vmin, vmax=vmax, origin='lower')
    # fonction à définir quand blit=True
    # crée l'arrière de l'animation qui sera présent sur chaque image
    def init():

        contf = ax.contourf(MAIN,Z,V[0],**kw)
        cb = plt.colorbar(contf)
        cb.set_label(cbarlabel, labelpad=10)
        #cb.set_label('$'+var+'$' + Vscalestr, labelpad=10)
        plt.xlabel('$x$ (km)')
        plt.ylabel('$z$ (m)')
        return()

    def animate(i):
        ax.clear()
        plt.contourf(MAIN,Z,V[i],**kw)
        print(np.round(i/frames*100,2),'%' , end="\r")

        plt.streamplot(stream_X,stream_Y,streamU[i]*1e-3,streamV[i],color = "k",linewidth = 0.8,density = (0.5,0.5),arrowsize = 0.8,minlength = 1)
        plt.streamplot(stream_X,stream_Y,streamU[i]*1e-3,streamV[i],color = "k",linewidth = 0.8,density = (0.5,0.5),arrowsize = 0.001,maxlength = 0.8)

        plt.plot(MAIN[0],topography[i],'k',linewidth = 3)
        plt.ylim(np.min(Z),np.max(Z))
        plt.xlim(np.min(MAIN),np.max(MAIN))
        return(),

    ani = animation.FuncAnimation(fig, animate, init_func=init, frames=frames, blit=False, interval=20, repeat=False)
    ani.save(filename, fps=24,dpi=50)

    #
    # # Génération de l'animation, frames précise les arguments numérique reçus par func (ici animate),
    # # interval est la durée d'une image en ms, blit gère la mise à jour
    # ani = animation.FuncAnimation(fig=fig, func=animate, frames=range(frames), interval=50, blit=True)
    # ani.save(filename="courbe.mp4", dpi =80, fps=20)
    plt.show()


def animation_particles(topo,dic,X,Y,Z,time,zev,zeta,lfunc_mantle,lfunc_core,funcstream_mantle,funcstream_core,di = 'x',cmap = 'viridis',cbarlabel = '/',**kwargs):
    "plot a cross section of choosen scalar field (var) + streamlines (varstream) "
    topo = lambdify([x,y,t,et],re(topo))
    Xscale = np.float64(mp.re(dic['Rl']*2890000))

    V = Field(topo,lfunc_mantle,lfunc_core,X,Y,Z,time,zev,zeta)
    topography = topo(X[0],Y[0],time,zeta)

    if di == 'x':
        MAIN = X
    if di == 'y':
        MAIN = Y

#######################################
    if 'vmin' in kwargs.keys():
        vmin = kwargs['vmin']
    else:
        vmin = np.nanmin(V)

    if 'vmax' in kwargs.keys():
        vmax = kwargs['vmax']
    else:
        vmax = np.nanmax(V)
##########################################################################
# Création de la figure et de l'axe
    frames =320

    fig, ax = plt.subplots(figsize=(15,6))

    ################## plot background ##################
    plt.contourf(MAIN,Z,V,30,cmap = cmap,vmin = vmin,vmax=vmax)
    cb = plt.colorbar()
    cb.set_label(cbarlabel, labelpad=10)

    ########################################################################
    ##############position of particles at depart
    dt=5e2

    density = (20,20)
    places = np.array([[random.uniform(MAIN[0,0], MAIN[0,-1]) for i in range(density[0])],[random.uniform(Z[0,0], Z[-1,0]) for i in range(density[1])]])
    okpos = places[1] < topo(places[0],0,time,zeta)
    places = np.array([places[0][okpos],places[1][okpos]])

    ########################################################################

    ################### plot points of depart ##############################

    point = ax.scatter(places[0], places[1],marker ='d',s = 10,c= '#216B41')

    plt.plot(MAIN[0],topography,'k',linewidth = 3)

    plt.xlabel('$x$ (km)')
    plt.ylabel('$z$ (m)')
    plt.ylim(np.min(Z),np.max(Z))
    plt.xlim(np.min(MAIN),np.max(MAIN))
    # Création de la function qui sera appelée à "chaque nouvelle image"
    count=0
    def animate(k):
        data = point.get_offsets()
        xo = data.T[0]
        yo = data.T[1]
        xnew = []
        ynew = []
        for i in range(len(xo)):
            if yo[i] < topo(xo[i],0,time,zeta) or xo[i] < (MAIN[0,-1]) or xo[i] > (MAIN[0,0]):
                vx = funcstream_core[0](xo[i],0,yo[i],time,zev,zeta)
                vy = funcstream_core[1](xo[i],0,yo[i],time,zev,zeta)
                xo[i] += vx*dt
                yo[i] += vy*dt
                if yo[i] <= topo(xo[i],0,time,zeta) or xo[i] < (MAIN[0,-1]) or xo[i] > (MAIN[0,0]):

                    xnew.append(xo[i])
                    ynew.append(yo[i])

        ############ FANCYNESS #################
        colors=[]
        for c in range(len(xnew)):
            if random.random() < 0.1:
                color = random.choice(['#184D2F','#20663E','#28804E','#2F995D','#37B36D'])
                colors.append(color)
        point.set_color(colors)

        if random.random() < 0.4:
            addx = np.array([random.uniform(MAIN[0,0], MAIN[0,0]) for i in range(int(density[0]/2))])
            addy = np.array([random.uniform(Z[0,0], Z[-1,0]) for i in range(int(density[1]/2))])

            okpos = addy < topo(addx,0,time,zeta)


            xnew += list(addx[okpos])
            ynew += list(addy[okpos])
        t = mpl.markers.MarkerStyle(marker='d')
        t._transform = t.get_transform().rotate_deg(k*3)
    #     point.set_marker((3, 0, k*5))
        # point.set_marker(t)
        point.set_offsets(np.array([xnew,ynew]).T)
        return()

    # Génération de l'animation, frames précise les arguments numérique reçus par func (ici animate),
    # interval est la durée d'une image en ms, blit gère la mise à jour
    ani = animation.FuncAnimation(fig=fig, func=animate, frames=range(frames), interval=50, blit=True)
    ani.save(filename="courbe.mp4", dpi =80, fps=20)
    plt.show()





def plot_topo3D(topo,xn,yn,tn,zeta,dic,cmap = 'viridis'):
    topo = lambdify([x,y,t,et],re(topo))
    Xscale = np.float64(mp.re(dic['Rl']*2890000))

    X,Y = np.meshgrid(xn,yn)


    fig = plt.figure(figsize = (12,12))
    ax = fig.add_subplot(111, projection='3d')
    Z = topo(X,Y,tn,zeta)
    ax.contour(X*Xscale*1e-3, Y*Xscale*1e-3, Z*Xscale*1e-3, 10, colors = 'k', offset=-4*Xscale*1e-3*zeta)

    ax.set_zlim(-zeta*4*Xscale*1e-3,zeta*3.5*Xscale*1e-3)
    ax.view_init(30,)
    #ax.set_axis_off()
    ax.set_xlabel('$x$ (km)',labelpad =30)
    ax.set_ylabel('$y$ (km)',labelpad =30)
    ax.set_zlabel('$z$ (km)',labelpad =30)


    # Plot the surface.
    surf = ax.plot_surface(X*Xscale*1e-3, Y*Xscale*1e-3, Z*Xscale*1e-3, cmap=cmap,
                           linewidth=0, zorder=1,ccount = 70,rcount=70)
def plot_topo_cut(topo,xn,tn,zeta,dic,cmap = 'viridis'):
    from matplotlib.path import Path
    from matplotlib.patches import PathPatch
    topo = lambdify([x,y,t,et],re(topo))
    # Xscale = np.float64(mp.re(dic['Rl']*2890000))
    fig = plt.figure(figsize = (12,5))

    zn= np.real(topo(xn,0,tn,zeta))
    zn2 = np.real(topo(xn,np.pi,tn,zeta))

    xn = xn*Xscale*1e-3
    zn = zn*Xscale*1e-3
    zn2 = zn2*Xscale*1e-3

    plt.plot(xn,zn,'k',alpha=0.7)
    #plt.plot(xn,zn2,'k',alpha = 0.8)
    ym = zeta*Xscale*1.2*1e-3

    pa2 = np.array([xn,zn2]).T
    pa2 = np.vstack((np.array([np.min(xn),ym]),pa2))
    pa2 = np.vstack((pa2,np.array([np.max(xn),ym])))

    pa = np.array([xn,zn]).T
    pa = np.vstack((np.array([np.min(xn),ym]),pa))
    pa = np.vstack((pa,np.array([np.max(xn),ym])))

    path = Path(pa2)
    patch = PathPatch(path, facecolor='none',edgecolor='none')
    plt.gca().add_patch(patch)
    im = plt.imshow(xn.reshape(zn2.size,1),alpha = 0.8,  cmap=cmap,interpolation="bicubic",
                    origin='lower',extent=[-10*Xscale,10*Xscale,-1*Xscale*zeta*1e-3,ym],aspect="auto", clip_path=patch, clip_on=True)



    path = Path(pa)
    patch = PathPatch(path, facecolor='none',edgecolor='none')
    plt.gca().add_patch(patch)
    im = plt.imshow(xn.reshape(zn.size,1), cmap=cmap,interpolation="bicubic",
                    origin='lower',extent=[-10*Xscale,10*Xscale,-1*Xscale*zeta*1e-3,ym],aspect="auto", clip_path=patch, clip_on=True,alpha=1)



    plt.ylim(-zeta*Xscale*2*1e-3,ym)
    plt.xlim(np.min(xn),np.max(xn))
    plt.xlabel('$x$ (km)')
    plt.ylabel('$z$ (km)')
